# Falafel-Döner

Link to repository: [GitLab](https://gitlab.com/Rustacian/falafel-doener)

## Requirements

- [HateList](https://legacy.curseforge.com/rom/addons/hatelist)

## Roadmap

- Champion
  - Rogue
    - fill up rage on shortage
    - remote only script
    - tank script
    - abort casts of enemies
    - add fear handler
      - cancel fear if possible
      - kneel if not
    - detect if not facing target, then use MoveBackwardStart
- Warlock
  - Champion
    - dd script
- Buffrota
  - **Option 1**: Buff dependent buffing
  - Option 2: Countdown message depending buffing
    - must adapt to different countdowns
    - more complex
- Encapsulate timer
