local FalafelDoener = _G.FalafelDoener
local runtime_data = FalafelDoener.runtime_data
local indices = FalafelDoener.indices
local Buffs = FalafelDoener.Buffs
local Skills = FalafelDoener.Skills
local Targets = FalafelDoener.Targets
local s_ids = Skills.ids
local buff_ids = Buffs.ids
local names = Skills.names

local durations_buff = runtime_data[indices.BuffDuration]
local stacks_buff = runtime_data[indices.BuffStack]
local buff_exists = runtime_data[indices.BuffExists]
local debuff_exists = runtime_data[indices.DebuffExists]
local params_buff =  runtime_data[indices.BuffParams]
local durations_debuff =  runtime_data[indices.DebuffDuration]
local stacks_debuff =  runtime_data[indices.DebuffStack]
local params_debuff =  runtime_data[indices.DebuffParams]
local skill_tab_ids = runtime_data[indices.SkillTabIDs]
local skill_slots = runtime_data[indices.SkillSlots]
local set_skill_action_slots = runtime_data[indices.SUIT_SKILL_ACTION_SLOTS]
local skill_action_slots = runtime_data[indices.SKILL_ACTION_SLOTS]

local PLAYER = "player"
local TARGET = "target"

local UnitMana = UnitMana
local UnitSkill = UnitSkill
local UnitHealth = UnitHealth
local UnitMaxHealth = UnitMaxHealth
local UnitBuff = UnitBuff
local UnitBuffLeftTime = UnitBuffLeftTime
local UnitDebuff = UnitDebuff
local UnitDebuffLeftTime = UnitDebuffLeftTime
local print = print
local GetPlayerCombatState = GetPlayerCombatState
local UnitCastingTime = UnitCastingTime

local CastSpellByName_wrapper = Skills.CastSpellByName_wrapper
local Update_current = Buffs.Update_current
local Skill_cooldown = Skills.Get_skill_cooldown
local Suit_skill_cooldown = Skills.Get_suit_skill_cooldown
local Get_skill_usable_on_action_bar = Skills.Get_skill_usable_on_action_bar
local AutoTarget = Targets.AutoTarget
local TargetRelativeHealth = Targets.TargetRelativeHealth

local                     ATTACK_NAME             =                           names[s_ids.ATTACK]
local                     POISONOUS_NAME          =                           names[s_ids.POISONOUS_WARLOCK]
local THROW,              THROW_NAME              = s_ids.THROW,              names[s_ids.THROW]
local COMBO_THROW,        COMBO_THROW_NAME        = s_ids.COMBO_THROW,        names[s_ids.COMBO_THROW]
local WARP_CHARGE,        WARP_CHARGE_NAME        = s_ids.WARP_CHARGE,        names[s_ids.WARP_CHARGE]
local PREMEDITATION,      PREMEDITATION_NAME      = s_ids.COMBO_THROW,        names[s_ids.PREMEDITATION]
local WOUND_ATTACK,       WOUND_ATTACK_NAME       = s_ids.WOUND_ATTACK,       names[s_ids.WOUND_ATTACK]
local LOW_BLOW,           LOW_BLOW_NAME           = s_ids.LOW_BLOW,           names[s_ids.LOW_BLOW]
local SHADOWSTAB,         SHADOWSTAB_NAME         = s_ids.SHADOWSTAB,         names[s_ids.SHADOWSTAB]
local ENERGY_CONSERVATION,ENERGY_CONSERVATION_NAME= s_ids.ENERGY_CONSERVATION,names[s_ids.ENERGY_CONSERVATION]
local GHOSTLY_STRIKE,     GHOSTLY_STRIKE_NAME     = s_ids.GHOSTLY_STRIKE,     names[s_ids.GHOSTLY_STRIKE]
local SOUL_STAB,          SOUL_STAB_NAME          = s_ids.SOUL_STAB,          names[s_ids.SOUL_STAB]

local WARP_CHARGE_BUFF  = buff_ids.WARP_CHARGE
local BLEED             = buff_ids.BLEED
local GRIEVOUS_WOUND    = buff_ids.GRIEVOUS_WOUND
local POISONOUS         = buff_ids.POISONOUS_WARLOCK
local WEAKENED          = buff_ids.WEAKENED

local tolerance = 0.41
local function cd_done(id)
    return Skill_cooldown(skill_tab_ids, skill_slots, id) < tolerance
end
local function suit_cd_done(id)
    return Suit_skill_cooldown(set_skill_action_slots, id) < tolerance
end


local function rogue_warlock_distance(target_relative_health, focus)
    if Get_skill_usable_on_action_bar(skill_action_slots, THROW) then
        CastSpellByName_wrapper(THROW_NAME)
    elseif focus > 20 and
           durations_buff[WARP_CHARGE_BUFF] < 2 and
           cd_done(WARP_CHARGE) then
        CastSpellByName_wrapper(WARP_CHARGE_NAME)
    elseif cd_done(THROW) then
        CastSpellByName_wrapper(THROW_NAME)
    elseif cd_done(COMBO_THROW) then
        CastSpellByName_wrapper(COMBO_THROW_NAME)
    else
        return false
    end
    return true
end

function FalafelDoener.Attack_rogue_warlock() FalafelDoener.Skills.ToggleLogging()
    Update_current(
        durations_buff,
        stacks_buff,
        params_buff,
        UnitBuff,
        UnitBuffLeftTime,
        PLAYER
    )
    Update_current(
        durations_debuff,
        stacks_debuff,
        params_debuff,
        UnitDebuff,
        UnitDebuffLeftTime,
        TARGET
    )

    local player_relative_health = UnitHealth(PLAYER) / UnitMaxHealth(PLAYER)
    local energy = UnitMana(PLAYER)
    local focus = UnitSkill(PLAYER)
    local target_relative_health = TargetRelativeHealth()
    local target_invalid = not target_relative_health
    local in_combat = GetPlayerCombatState()

    if target_invalid then
        AutoTarget()
        target_relative_health = TargetRelativeHealth()
        target_invalid = not target_relative_health
    end

    local cast_name, cast_duration, cast_time_done = UnitCastingTime(PLAYER)
    local cast_done = 
        cast_name == nil or 
        ( (cast_duration - cast_time_done) < .1 )

    if not target_invalid and (
       not cd_done(SHADOWSTAB) or
       (not cast_done and cast_name == COMBO_THROW_NAME)
    ) then
        -- global cooldown active
        CastSpellByName_wrapper(ATTACK_NAME)
    elseif not target_invalid and
           energy >= 35 and
           debuff_exists[BLEED] and
           debuff_exists[GRIEVOUS_WOUND] and
           cd_done(WOUND_ATTACK) then
        if cd_done(THROW) then
            CastSpellByName_wrapper(THROW_NAME)
        else
            CastSpellByName_wrapper(WOUND_ATTACK_NAME)
        end
    -- 0.75 - 7 * 0.03 (7 -> stops afterwards, not before - needs test)
    elseif player_relative_health <= 0.54 and
           cd_done(ENERGY_CONSERVATION) then
        CastSpellByName_wrapper(ENERGY_CONSERVATION_NAME)
    -- TODO: detect movement (can't be cast during movement)
    elseif (
                not buff_exists[POISONOUS] or
                durations_buff[POISONOUS] < 10
           ) and
           (
                not in_combat or
                target_invalid or not (
                    -- wound attack rotation could be started
                    energy >= 85 or
                    -- 1 second can generate up to ten focus in a fight -> don't waste focus generation
                    focus >= 90 or
                    (debuff_exists[BLEED] and energy >= 65)
                    -- Wound Attack is already caputed above -> capture low blow as well?
                )
           ) then
        CastSpellByName_wrapper(POISONOUS_NAME)
    elseif energy == 100 and
           not in_combat and (
                (
                    not Get_skill_usable_on_action_bar(skill_action_slots, WARP_CHARGE) and
                    not buff_exists[WARP_CHARGE_BUFF]
                ) or
                target_invalid
            )
            then
        CastSpellByName_wrapper(PREMEDITATION_NAME)
    elseif target_invalid then
        return
    elseif Get_skill_usable_on_action_bar(skill_action_slots, THROW) then
        CastSpellByName_wrapper(THROW_NAME)
    elseif focus >= 30 and
           not buff_exists[WARP_CHARGE_BUFF] or
           -- ensure SLW rotation can be performed within the remaining time 
           durations_buff[WARP_CHARGE_BUFF] < 3 then
        CastSpellByName_wrapper(WARP_CHARGE_NAME)
    -- requires energy at least 30 or 65 (following Wound Attack), however implicitly checked by Shadowstab
    elseif cd_done(THROW) then
        CastSpellByName_wrapper(THROW_NAME)
    elseif debuff_exists[BLEED] and
           -- can fail if Wound Attack on cooldown 
           not debuff_exists[GRIEVOUS_WOUND] and
           -- could e.g. fail when 
           durations_debuff[BLEED] > 8 then
        CastSpellByName_wrapper(LOW_BLOW_NAME)
    -- requires implicitly a cooldown < 2 on Wound Attack, however implicitly guaranteed by energy requirements
    -- 85 energy is required for performing SLW in succession sequence
    elseif Skill_cooldown(skill_tab_ids, skill_slots, SHADOWSTAB) == 0 and
           not Get_skill_usable_on_action_bar(skill_action_slots, SHADOWSTAB) and
           energy >= 20 then
        print("remote")
        if not rogue_warlock_distance(target_relative_health, focus) then
            CastSpellByName_wrapper(ATTACK_NAME)
        end
    elseif energy >= 85 then
        CastSpellByName_wrapper(SHADOWSTAB_NAME)
    -- 30 focus for skill and 30 for warp charge and - 3 minimum generation for ~ 2 - 3 seconds buffer
    elseif focus >= 57 and
           cd_done(SOUL_STAB) and
           debuff_exists[WEAKENED] then
        CastSpellByName_wrapper(SOUL_STAB_NAME)
    elseif focus >= 57 and
           not debuff_exists[WEAKENED] and
           cd_done(GHOSTLY_STRIKE) then
        CastSpellByName_wrapper(GHOSTLY_STRIKE_NAME)
    else
        CastSpellByName_wrapper(ATTACK_NAME)
    end
end