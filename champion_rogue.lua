local FalafelDoener = _G.FalafelDoener
local runtime_data = FalafelDoener.runtime_data
local indices = FalafelDoener.indices
local Buffs = FalafelDoener.Buffs
local Skills = FalafelDoener.Skills
local Targets = FalafelDoener.Targets
local skill_ids = Skills.ids
local buff_ids = Buffs.ids
local names = Skills.names

local durations_buff = runtime_data[indices.BuffDuration]
local stacks_buff = runtime_data[indices.BuffStack]
local buff_exists = runtime_data[indices.BuffExists]
local debuff_exists = runtime_data[indices.DebuffExists]
local params_buff =  runtime_data[indices.BuffParams]
local durations_debuff =  runtime_data[indices.DebuffDuration]
local stacks_debuff =  runtime_data[indices.DebuffStack]
local params_debuff =  runtime_data[indices.DebuffParams]
local skill_tab_ids = runtime_data[indices.SkillTabIDs]
local skill_slots = runtime_data[indices.SkillSlots]
local set_skill_action_slots = runtime_data[indices.SUIT_SKILL_ACTION_SLOTS]
local skill_action_slots = runtime_data[indices.SKILL_ACTION_SLOTS]

local UnitMana = UnitMana
local UnitSkill = UnitSkill
local UnitHealth = UnitHealth
local UnitMaxHealth = UnitMaxHealth
local UnitBuff = UnitBuff
local UnitBuffLeftTime = UnitBuffLeftTime
local UnitDebuff = UnitDebuff
local UnitDebuffLeftTime = UnitDebuffLeftTime
local CastSpellByName = CastSpellByName
local TargetNearestEnemy = TargetNearestEnemy
local print = print

local CastSpellByName_wrapper = Skills.CastSpellByName_wrapper
local Update_current = Buffs.Update_current
local Skill_cooldown = Skills.Get_skill_cooldown
local Suit_skill_cooldown = Skills.Get_suit_skill_cooldown
local Get_skill_usable_on_action_bar = Skills.Get_skill_usable_on_action_bar
local EnemyTargetsMe = Targets.EnemyTargetsMe
local AutoTarget = Targets.AutoTarget
local TargetRelativeHealth = Targets.TargetRelativeHealth

local FEARLESS_BLOWS = skill_ids.FEARLESS_BLOWS
local FORGE_BUFF = buff_ids.FORGE_BUFF
local THROW = skill_ids.THROW
local FEROCIOUS_DISASSEMBLY = skill_ids.FEROCIOUS_DISASSEMBLY
local ELECTROCUTION = skill_ids.ELECTROCUTION
local ENERGY_INFLUX_STRIKE = skill_ids.ENERGY_INFLUX_STRIKE
local RUNE_GROWTH = skill_ids.RUNE_GROWTH
local FORGE = skill_ids.FORGE
local SHADOW_STEP = skill_ids.SHADOW_STEP
local SHADOWSTAB = skill_ids.SHADOWSTAB_CHAMPION
local SHADOW_PULSE = skill_ids.SHADOW_PULSE
local PUNISHERS_DISASSEMBLY = skill_ids.PUNISHERS_DISASSEMBLY
local RUNE_PULSE = skill_ids.RUNE_PULSE

local CHAIN_DRIVE = buff_ids.CHAIN_DRIVE
local ATTACK_RUNE_GROWTH_BUFF = buff_ids.ATTACK_RUNE_GROWTH_BUFF
local DISASSEMBLING_STAGE = buff_ids.DISASSEMBLING_STAGE
local ELECTROCUTION_BUFF = buff_ids.ELECTROCUTION_BUFF
local DISASSEMBLY_MODE = buff_ids.DISASSEMBLY_MODE
local SHIELD_FORM_BUFF = buff_ids.SHIELD_FORM
local DEATH_ARRIVES_BUFF = buff_ids.DEATH_ARRIVES
local HEAVY_BASH_BUFF = buff_ids.HEAVY_BASH_BUFF
local CHAIN_DRIVE_WARRIOR = buff_ids.CHAIN_DRIVE_WARRIOR

local RUNE_GROWTH_NAME = names[RUNE_GROWTH]
local FEARLESS_BLOWS_NAME = names[FEARLESS_BLOWS]
local FORGE_NAME = names[FORGE]
local THROW_NAME = names[THROW]
local ELECTROCUTION_NAME = names[ELECTROCUTION]
local FEROCIOUS_DISASSEMBLY_NAME = names[FEROCIOUS_DISASSEMBLY]
local SHADOW_STEP_NAME = names[SHADOW_STEP]
local ENERGY_INFLUX_STRIKE_NAME = names[ENERGY_INFLUX_STRIKE]
local SHADOWSTAB_NAME = names[SHADOWSTAB]
local SHADOW_PULSE_NAME = names[SHADOW_PULSE]
local RUNE_PULSE_NAME = names[RUNE_PULSE]
local PUNISHERS_DISASSEMBLY_NAME = names[skill_ids.PUNISHERS_DISASSEMBLY]
local HEAVY_BASH_NAME = names[skill_ids.HEAVY_BASH]
local ATTACK_NAME = names[skill_ids.ATTACK_CHAMPION]
local SHIELD_FORM_NAME = names[skill_ids.SHIELD_FORM]

local tolerance = 0.41
local function cd_done(id)
    return Skill_cooldown(skill_tab_ids, skill_slots, id) < tolerance
end
local function suit_cd_done(id)
    return Suit_skill_cooldown(set_skill_action_slots, id) < tolerance
end


local function champion_rogue_distance(target_relative_health, rage)
    if Get_skill_usable_on_action_bar(skill_action_slots, THROW) then
        CastSpellByName_wrapper(THROW_NAME)
    elseif target_relative_health < .45
           and Get_skill_usable_on_action_bar(skill_action_slots, FEARLESS_BLOWS) then
        CastSpellByName_wrapper(FEARLESS_BLOWS_NAME)
    elseif cd_done(SHADOW_PULSE) then -- the distance might be larger than the range in addition to cooldown, but the character will then automatically move towards the target
        CastSpellByName_wrapper(SHADOW_PULSE_NAME)
    elseif rage > 15 and cd_done(FEARLESS_BLOWS) then
        CastSpellByName_wrapper(FEARLESS_BLOWS_NAME)
    elseif cd_done(THROW) then
        CastSpellByName_wrapper(THROW_NAME)
    else
        return false
    end
    return true
end

local PLAYER = "player"
local TARGET = "target"
local TARGET_TARGET = "targettarget"
local UnitCanAttack = UnitCanAttack
function FalafelDoener.Attack_champion_rogue_dd_demo()
    Update_current(
        durations_buff,
        stacks_buff,
        params_buff,
        UnitBuff,
        UnitBuffLeftTime,
        PLAYER
    )
    Update_current(
        durations_debuff,
        stacks_debuff,
        params_debuff,
        UnitDebuff,
        UnitDebuffLeftTime,
        TARGET
    )

    local rage = UnitMana(PLAYER)
    local energy = UnitSkill(PLAYER)
    local target_relative_health = TargetRelativeHealth()
    -- TODO: interrupt skills
    -- TODO: use pots
    -- TODO: don't attack when physical imune

    local target_invalid = not target_relative_health
    if target_invalid then
        AutoTarget()
        target_relative_health = TargetRelativeHealth()
        target_invalid = not target_relative_health
    end

    local distance_greater_50_to_target = 
        not target_invalid and
        Skill_cooldown(skill_tab_ids, skill_slots, SHADOWSTAB) == 0 and
        not Get_skill_usable_on_action_bar(skill_action_slots, SHADOWSTAB) and
        energy >= 20

        if not target_invalid and not cd_done(FORGE) then
        -- global cooldown active
        CastSpellByName_wrapper(ATTACK_NAME)
    -- cooldown satisfied implicitly as 4 stages require at least 20 seconds, but there could be multiple champions adding buffs
    elseif not distance_greater_50_to_target and
           buff_exists[CHAIN_DRIVE] and
           stacks_debuff[DISASSEMBLING_STAGE] == 4 and
           suit_cd_done(PUNISHERS_DISASSEMBLY) then
        CastSpellByName_wrapper(PUNISHERS_DISASSEMBLY_NAME)
    -- check cd because sometimes it happens that the full cd is active despite buff
    elseif not distance_greater_50_to_target and
           rage >= 70 and
           --buff_exists[DISASSEMBLY_MODE] and
           suit_cd_done(FEROCIOUS_DISASSEMBLY) then
        CastSpellByName_wrapper(FEROCIOUS_DISASSEMBLY_NAME)
    elseif not target_invalid and 
           buff_exists[CHAIN_DRIVE] and
           cd_done(RUNE_PULSE) then
        CastSpellByName_wrapper(RUNE_PULSE_NAME)
    elseif not buff_exists[FORGE_BUFF] or
           durations_buff[FORGE_BUFF] < 5 then
        CastSpellByName_wrapper(FORGE_NAME)
    elseif not buff_exists[ATTACK_RUNE_GROWTH_BUFF] and
           cd_done(RUNE_GROWTH) then
        CastSpellByName_wrapper(names[RUNE_GROWTH])
    elseif target_invalid then
        return
    elseif buff_exists[DEATH_ARRIVES_BUFF] and cd_done(SHADOW_STEP) then
        CastSpellByName_wrapper(names[SHADOW_STEP])
    -- detect range issue implicitly by excluding other reasons which may be responsible for the skill not being usable
    -- alternatively to cooldown check: check if shadow pulse is usable
    elseif distance_greater_50_to_target then
        if Get_skill_usable_on_action_bar(skill_action_slots, SHADOW_STEP) then
            CastSpellByName_wrapper(names[SHADOW_STEP])
        elseif not champion_rogue_distance(target_relative_health, rage) then
            CastSpellByName_wrapper(ATTACK_NAME)
        end
        return
    elseif rage >= 75 and
           debuff_exists[HEAVY_BASH_BUFF] and
           stacks_debuff[HEAVY_BASH_BUFF] == 3 and
           durations_debuff[HEAVY_BASH_BUFF] < 2.5 then
        CastSpellByName_wrapper(HEAVY_BASH_NAME)
    elseif rage >= 65 and
           --buff_exists[DISASSEMBLY_MODE] and
           suit_cd_done(FEROCIOUS_DISASSEMBLY) then
        CastSpellByName_wrapper(FEROCIOUS_DISASSEMBLY_NAME) -- cast even before rune pulse?
    elseif cd_done(THROW) then
        CastSpellByName_wrapper(THROW_NAME)
    elseif rage >= 15 and
           target_relative_health < .45 and
           cd_done(FEARLESS_BLOWS) then
        CastSpellByName_wrapper(FEARLESS_BLOWS_NAME)
    elseif rage >= 90 and
           ( not debuff_exists[HEAVY_BASH_BUFF] or
             stacks_debuff[HEAVY_BASH_BUFF] < 3 ) then
        CastSpellByName_wrapper(HEAVY_BASH_NAME)
    elseif energy >= 50 then
        CastSpellByName_wrapper(SHADOWSTAB_NAME)
    elseif rage >= 15 and
           cd_done(FEARLESS_BLOWS) then
        CastSpellByName_wrapper(FEARLESS_BLOWS_NAME)
    elseif rage >= 95 and
           debuff_exists[ELECTROCUTION_BUFF] and
           cd_done(ENERGY_INFLUX_STRIKE) then
        CastSpellByName_wrapper(names[ENERGY_INFLUX_STRIKE])
    elseif rage >= 90 and
           cd_done(ELECTROCUTION) then
        CastSpellByName_wrapper(names[ELECTROCUTION])
    elseif energy >= 20 then
        CastSpellByName_wrapper(SHADOWSTAB_NAME)
    elseif rage >= 90 then
        CastSpellByName_wrapper(HEAVY_BASH_NAME)
    else
        CastSpellByName_wrapper(ATTACK_NAME)
    end
end

local GetPlayerCombatState = GetPlayerCombatState
local UnitIsUnit = UnitIsUnit
local function danger()
    local in_combat = GetPlayerCombatState()
    if not in_combat then
        return false
    end
    local enemy_targets_me = EnemyTargetsMe()
    if enemy_targets_me == nil then
        return true
    else
        return enemy_targets_me or (
            UnitHealth(TARGET) ~= 0 and
            UnitIsUnit(PLAYER, TARGET_TARGET)
        )
    end
end

function FalafelDoener.Attack_champion_rogue_dd_shield()
    Update_current(
        durations_buff,
        stacks_buff,
        params_buff,
        UnitBuff,
        UnitBuffLeftTime,
        PLAYER
    )
    Update_current(
        durations_debuff,
        stacks_debuff,
        params_debuff,
        UnitDebuff,
        UnitDebuffLeftTime,
        TARGET
    )

    local rage = UnitMana(PLAYER)
    local energy = UnitSkill(PLAYER)
    local target_relative_health = TargetRelativeHealth()
    -- TODO: interrupt skills
    -- TODO: use pots
    -- TODO: don't attack when physical imune

    local target_invalid = not target_relative_health
    if target_invalid then
        AutoTarget()
        target_relative_health = TargetRelativeHealth()
        target_invalid = not target_relative_health
    end
    if not target_invalid and not cd_done(FORGE) then -- global cooldown active
        CastSpellByName_wrapper(ATTACK_NAME)
    elseif not buff_exists[FORGE_BUFF] or
           durations_buff[FORGE_BUFF] < 3 then
        CastSpellByName_wrapper(FORGE_NAME)
    elseif not buff_exists[ATTACK_RUNE_GROWTH_BUFF] and
           not buff_exists[SHIELD_FORM_BUFF] and
           cd_done(RUNE_GROWTH) then
        CastSpellByName_wrapper(names[RUNE_GROWTH])
    --[[ see truth table:
        shield  rune    rcd     tt=self     switch
        1       1       1           1           0   read: if shield form active, rune buff active, the
        1       1       1           0           0   cooldown of rune grwoth is ready and the target 
        1       1       0           1           0   targets me, don't switch shield form
        1       1       0           0           0
        1       0       1           1           0
        1       0       1           0           1
        1       0       0           1           0
        1       0       0           0           0
        0       1       1           1           1   rune soon false
        0       1       1           0           0   rune soon false
        0       1       0           1           1
        0       1       0           0           1
        0       0       1           1           1   usually invalid, but could happen during casting rune growth 
        0       0       1           0           1   usually invalid, but could happen during casting rune growth
        0		0		0           1			1
        0		0	    0           0			1

        switch = 1:                                     terms
        1       0       1           0           1        2
        0       1       1           1           1        2
        0       1       0           1           1        1 12
        0       1       0           0           1        1 12
        0       0       1           1           1        1 11
        0       0       1           0           1        1 11
        0		0		0           1			1        1 11
        0		0	    0           0			1        1 11
        
        Result:          1             11        12                           2
        switch = (not shield and (not rune or not rcd)) or (rcd and shield == not rune and rune == tt)

        Alternatively, rune could be defined as (attack rune growth or defense rune growth) instead of only (attack rune growth)
    ]]--
    elseif (
                not buff_exists[SHIELD_FORM_BUFF] and
                (
                    not buff_exists[ATTACK_RUNE_GROWTH_BUFF] or
                    not cd_done(RUNE_GROWTH)
                )
           ) or
           (
                not not buff_exists[SHIELD_FORM_BUFF] == not not not buff_exists[ATTACK_RUNE_GROWTH_BUFF] and
                not not buff_exists[ATTACK_RUNE_GROWTH_BUFF] == danger() and
                cd_done(RUNE_GROWTH)
           ) then
        CastSpellByName_wrapper(SHIELD_FORM_NAME)
    elseif target_invalid then
        return
    elseif rage >= 80 and
           debuff_exists[HEAVY_BASH_BUFF] and
           stacks_debuff[HEAVY_BASH_BUFF] == 3 and
           durations_debuff[HEAVY_BASH_BUFF] < 2.5 then
        CastSpellByName_wrapper(HEAVY_BASH_NAME)
    -- cooldown satisfied implicitly as 4 stages require at least 20 seconds, but there could be multiple champions adding buffs
    elseif (buff_exists[CHAIN_DRIVE] or buff_exists[CHAIN_DRIVE_WARRIOR]) and
           stacks_debuff[DISASSEMBLING_STAGE] == 4 and
           suit_cd_done(PUNISHERS_DISASSEMBLY) then
        CastSpellByName_wrapper(names[PUNISHERS_DISASSEMBLY])
    -- check cd because sometimes it happens that the full cd is active despite buff
    elseif (buff_exists[CHAIN_DRIVE] or buff_exists[CHAIN_DRIVE_WARRIOR]) and
           cd_done(RUNE_PULSE) then
        CastSpellByName_wrapper(RUNE_PULSE_NAME)
    elseif buff_exists[DEATH_ARRIVES_BUFF] and
           energy >= 20 and 
           cd_done(SHADOW_STEP) then
        CastSpellByName_wrapper(names[SHADOW_STEP])
    elseif Skill_cooldown(skill_tab_ids, skill_slots, SHADOWSTAB) == 0 and
           not Get_skill_usable_on_action_bar(skill_action_slots, SHADOWSTAB) and
           energy >= 20 then
        if Get_skill_usable_on_action_bar(skill_action_slots, SHADOW_STEP) then
            CastSpellByName_wrapper(names[SHADOW_STEP])
        elseif not champion_rogue_distance(target_relative_health, rage) then
            CastSpellByName_wrapper(ATTACK_NAME)
        end
        return
    elseif cd_done(THROW) then
        CastSpellByName_wrapper(THROW_NAME)
    elseif rage >= 15 and
           target_relative_health < .45 and
           cd_done(FEARLESS_BLOWS) then
        CastSpellByName_wrapper(names[FEARLESS_BLOWS])
    elseif rage >= 90 and
           ( not debuff_exists[HEAVY_BASH_BUFF] or
             stacks_debuff[HEAVY_BASH_BUFF] < 3 ) then
        CastSpellByName_wrapper(HEAVY_BASH_NAME)
    elseif energy >= 50 then
        CastSpellByName_wrapper(SHADOWSTAB_NAME)
    elseif rage >= 15 and
           cd_done(FEARLESS_BLOWS) then
        CastSpellByName_wrapper(names[FEARLESS_BLOWS])
    elseif rage >= 95 and
           debuff_exists[ELECTROCUTION_BUFF] and
           cd_done(ENERGY_INFLUX_STRIKE) then
        CastSpellByName_wrapper(names[ENERGY_INFLUX_STRIKE])
    elseif rage >= 90 and
           cd_done(ELECTROCUTION) then
        CastSpellByName_wrapper(names[ELECTROCUTION])
    elseif energy >= 20 then
        CastSpellByName_wrapper(SHADOWSTAB_NAME)
    elseif rage >= 90 then
        CastSpellByName_wrapper(HEAVY_BASH_NAME)
    else
        CastSpellByName_wrapper(ATTACK_NAME)
    end
end

function FalafelDoener.Attack_champion_rogue_distance()
    Update_current(
        durations_buff,
        stacks_buff,
        params_buff,
        UnitBuff,
        UnitBuffLeftTime,
        PLAYER
    )

    local rage = UnitMana(PLAYER)
    local target_relative_health = TargetRelativeHealth()
    local target_invalid = not target_relative_health
    if not target_invalid and buff_exists[CHAIN_DRIVE] then
        CastSpellByName_wrapper(RUNE_PULSE_NAME)
    elseif not buff_exists[FORGE_BUFF] or
           durations_buff[FORGE_BUFF] < 3 then
        CastSpellByName_wrapper(FORGE_NAME)
    elseif not buff_exists[ATTACK_RUNE_GROWTH_BUFF] and cd_done(RUNE_GROWTH) then
        CastSpellByName_wrapper(RUNE_GROWTH_NAME)
    elseif target_invalid then
        return
    elseif not champion_rogue_distance(target_relative_health, rage) and cd_done(RUNE_PULSE) then
        CastSpellByName_wrapper(RUNE_PULSE_NAME)
    end
end
