runtime_data.lua

lib/timer.lua
lib/name_mapping.lua
lib/skills/skills.lua
lib/buffs/buffs.lua
lib/items.lua
lib/targets.lua
lib/movement_detection.lua

event_handler.lua

champion_rogue.lua
champion_warrior.lua
champion_mage.lua
rogue_warlock.lua
warlock_rogue.lua

falafel_doener.lua
