local FalafelDoener = _G.FalafelDoener
local runtime_data = FalafelDoener.runtime_data
local indices = FalafelDoener.indices
local Buffs = FalafelDoener.Buffs
local Skills = FalafelDoener.Skills
local Targets = FalafelDoener.Targets
local skill_ids = Skills.ids
local buff_ids = Buffs.ids
local names = Skills.names

local durations_buff = runtime_data[indices.BuffDuration]
local stacks_buff = runtime_data[indices.BuffStack]
local buff_exists = runtime_data[indices.BuffExists]
local debuff_exists = runtime_data[indices.DebuffExists]
local params_buff =  runtime_data[indices.BuffParams]
local durations_debuff =  runtime_data[indices.DebuffDuration]
local stacks_debuff =  runtime_data[indices.DebuffStack]
local params_debuff =  runtime_data[indices.DebuffParams]
local skill_tab_ids = runtime_data[indices.SkillTabIDs]
local skill_slots = runtime_data[indices.SkillSlots]
local set_skill_action_slots = runtime_data[indices.SUIT_SKILL_ACTION_SLOTS]
local skill_action_slots = runtime_data[indices.SKILL_ACTION_SLOTS]

local UnitMana = UnitMana
local UnitSkill = UnitSkill
local UnitHealth = UnitHealth
local UnitBuff = UnitBuff
local UnitBuffLeftTime = UnitBuffLeftTime
local UnitDebuff = UnitDebuff
local UnitDebuffLeftTime = UnitDebuffLeftTime
local CastSpellByName = CastSpellByName
local print = print

local CastSpellByName_wrapper = Skills.CastSpellByName_wrapper
local Update_current = Buffs.Update_current
local Skill_cooldown = Skills.Get_skill_cooldown
local Suit_skill_cooldown = Skills.Get_suit_skill_cooldown
local Get_skill_usable_on_action_bar = Skills.Get_skill_usable_on_action_bar
local EnemyTargetsMe = Targets.EnemyTargetsMe
local TargetRelativeHealth = Targets.TargetRelativeHealth
local AutoTarget = Targets.AutoTarget

local FEARLESS_BLOW = skill_ids.FEARLESS_BLOW
local FORGE_BUFF = buff_ids.FORGE_MAGE
local FEROCIOUS_DISASSEMBLY = skill_ids.FEROCIOUS_DISASSEMBLY
local ELECTROCUTION = skill_ids.ELECTROCUTION
local ENERGY_INFLUX_STRIKE = skill_ids.ENERGY_INFLUX_STRIKE
local RUNE_GROWTH = skill_ids.RUNE_GROWTH
local FORGE = skill_ids.FORGE_MAGE
local SHADOW_STEP = skill_ids.SHADOW_STEP
local PUNISHERS_DISASSEMBLY = skill_ids.PUNISHERS_DISASSEMBLY
local RUNE_PULSE = skill_ids.RUNE_PULSE
local HIGH_ENERGY_BARRIER = skill_ids.HIGH_ENERGY_BARRIER
local HEAVY_BASH = skill_ids.HEAVY_BASH

local CHAIN_DRIVE = buff_ids.CHAIN_DRIVE
local ATTACK_RUNE_GROWTH_BUFF = buff_ids.ATTACK_RUNE_GROWTH_BUFF
local DISASSEMBLING_STAGE = buff_ids.DISASSEMBLING_STAGE
local ELECTROCUTION_BUFF = buff_ids.ELECTROCUTION_BUFF
local SHIELD_FORM_BUFF = buff_ids.SHIELD_FORM
local DEATH_ARRIVES_BUFF = buff_ids.DEATH_ARRIVES
local HEAVY_BASH_BUFF = buff_ids.HEAVY_BASH_BUFF
local CHAIN_DRIVE_WARRIOR = buff_ids.CHAIN_DRIVE_WARRIOR
local HIGH_ENERGY_BARRIER_BUFF = buff_ids.HIGH_ENERGY_BARRIER

local RUNE_GROWTH_NAME = names[RUNE_GROWTH]
local FORGE_NAME = names[FORGE]
local RUNE_PULSE_NAME = names[RUNE_PULSE]
local HEAVY_BASH_NAME = names[HEAVY_BASH]
local PUNISHERS_DISASSEMBLY_NAME = names[skill_ids.PUNISHERS_DISASSEMBLY]
local ATTACK_NAME = names[skill_ids.ATTACK]
local SHIELD_FORM_NAME = names[skill_ids.SHIELD_FORM]

local tolerance = 0.41
local function cd_done(id)
    return Skill_cooldown(skill_tab_ids, skill_slots, id) < tolerance
end
local function suit_cd_done(id)
    return Suit_skill_cooldown(set_skill_action_slots, id) < tolerance
end

local PLAYER = "player"
local TARGET = "target"
local TARGET_TARGET = "targettarget"
local UnitCanAttack = UnitCanAttack
function FalafelDoener.Attack_champion_mage_dd_demo()
    Update_current(
        durations_buff,
        stacks_buff,
        params_buff,
        UnitBuff,
        UnitBuffLeftTime,
        PLAYER
    )
    Update_current(
        durations_debuff,
        stacks_debuff,
        params_debuff,
        UnitDebuff,
        UnitDebuffLeftTime,
        TARGET
    )

    local rage = UnitMana(PLAYER)
    local mana = UnitSkill(PLAYER)
    local target_relative_health = TargetRelativeHealth()
    -- TODO: interrupt skills
    -- TODO: use pots
    -- TODO: don't attack when physical imune

    local target_invalid = not target_relative_health
    if target_invalid then
        AutoTarget()
        target_relative_health = TargetRelativeHealth()
        target_invalid = not target_relative_health
    end
    if not target_invalid and not cd_done(FORGE) then
        -- global cooldown active
        CastSpellByName_wrapper(ATTACK_NAME)
    -- cooldown satisfied implicitly as 4 stages require at least 20 seconds, but there could be multiple champions adding buffs
    elseif not target_invalid and
           buff_exists[CHAIN_DRIVE_WARRIOR] and
           stacks_debuff[DISASSEMBLING_STAGE] == 4 and
           suit_cd_done(PUNISHERS_DISASSEMBLY) then
        CastSpellByName_wrapper(PUNISHERS_DISASSEMBLY_NAME)
    -- check cd because sometimes it happens that the full cd is active despite buff
    elseif not target_invalid and
           buff_exists[CHAIN_DRIVE_WARRIOR] and
           cd_done(RUNE_PULSE) then
        CastSpellByName_wrapper(RUNE_PULSE_NAME)
    elseif rage >= 20 and
           debuff_exists[HEAVY_BASH_BUFF] and
           stacks_debuff[HEAVY_BASH_BUFF] == 3 and
           durations_debuff[HEAVY_BASH_BUFF] < 2.5 then
        CastSpellByName_wrapper(HEAVY_BASH_NAME)
    elseif not buff_exists[FORGE_BUFF] or
           durations_buff[FORGE_BUFF] < 3 then
        CastSpellByName_wrapper(FORGE_NAME)
    elseif not buff_exists[HIGH_ENERGY_BARRIER_BUFF] and
           cd_done(HIGH_ENERGY_BARRIER) then
        CastSpellByName_wrapper(names[HIGH_ENERGY_BARRIER])
    elseif not buff_exists[ATTACK_RUNE_GROWTH_BUFF] and
           cd_done(RUNE_GROWTH) then
        CastSpellByName_wrapper(names[RUNE_GROWTH])
    elseif target_invalid then
        return
    -- detect range issue implicitly by excluding other reasons which may be responsible for the skill not being usable
    -- alternatively to cooldown check: check if shadow pulse is usable
    elseif Skill_cooldown(skill_tab_ids, skill_slots, HEAVY_BASH) == 0 and
           not Get_skill_usable_on_action_bar(skill_action_slots, HEAVY_BASH) and
           rage >= 20 and
           cd_done(FEARLESS_BLOW) then
        CastSpellByName_wrapper(names[FEARLESS_BLOW])
    elseif rage >= 20 and
           ( not debuff_exists[HEAVY_BASH_BUFF] or
             stacks_debuff[HEAVY_BASH_BUFF] < 3 ) then
        CastSpellByName_wrapper(HEAVY_BASH_NAME)
    elseif rage >= 15 and
           cd_done(FEARLESS_BLOW) then
        CastSpellByName_wrapper(names[FEARLESS_BLOW])
    elseif rage >= 25 and
           debuff_exists[ELECTROCUTION_BUFF] and
           cd_done(ENERGY_INFLUX_STRIKE) then
        CastSpellByName_wrapper(names[ENERGY_INFLUX_STRIKE])
    elseif rage >= 45 and -- 45 required to execute combo with energy influx strike
           cd_done(ELECTROCUTION) then
        CastSpellByName_wrapper(names[ELECTROCUTION])
    elseif rage >= 45 then
        CastSpellByName_wrapper(HEAVY_BASH_NAME)
    else
        CastSpellByName_wrapper(ATTACK_NAME)
    end
end

local GetPlayerCombatState = GetPlayerCombatState
local UnitIsUnit = UnitIsUnit
local function danger()
    local in_combat = GetPlayerCombatState()
    if not in_combat then
        return false
    end
    local enemy_targets_me = EnemyTargetsMe()
    if enemy_targets_me == nil then
        return true
    else
        return enemy_targets_me or (
            UnitHealth(TARGET) ~= 0 and
            UnitIsUnit(PLAYER, TARGET_TARGET)
        )
    end
end

function FalafelDoener.Attack_champion_mage_dd_shield()
    Update_current(
        durations_buff,
        stacks_buff,
        params_buff,
        UnitBuff,
        UnitBuffLeftTime,
        PLAYER
    )
    Update_current(
        durations_debuff,
        stacks_debuff,
        params_debuff,
        UnitDebuff,
        UnitDebuffLeftTime,
        TARGET
    )

    local rage = UnitMana(PLAYER)
    local mana = UnitSkill(PLAYER)
    local target_relative_health = TargetRelativeHealth()
    local target_invalid = not target_relative_health

    -- TODO: abort skills
    -- TODO: use pots
    -- TODO: don't attack when physical imune
    if target_invalid then
        AutoTarget()
        target_relative_health = TargetRelativeHealth()
        target_invalid = not target_relative_health
    end


    if not target_invalid and not cd_done(FORGE) then -- global cooldown active
        CastSpellByName_wrapper(ATTACK_NAME)
    elseif not buff_exists[FORGE_BUFF] or
           durations_buff[FORGE_BUFF] < 3 then
        CastSpellByName_wrapper(FORGE_NAME)
    elseif not buff_exists[HIGH_ENERGY_BARRIER_BUFF] and
           cd_done(HIGH_ENERGY_BARRIER) then
        CastSpellByName_wrapper(names[HIGH_ENERGY_BARRIER])
    elseif not buff_exists[ATTACK_RUNE_GROWTH_BUFF] and
           not buff_exists[SHIELD_FORM_BUFF] and
           cd_done(RUNE_GROWTH) then
        CastSpellByName_wrapper(names[RUNE_GROWTH])
    --[[ see truth table:
        shield  rune    rcd     tt=self     switch
        1       1       1           1           0   read: if shield form active, rune buff active, the
        1       1       1           0           0   cooldown of rune grwoth is ready and the target 
        1       1       0           1           0   targets me, don't switch shield form
        1       1       0           0           0
        1       0       1           1           0
        1       0       1           0           1
        1       0       0           1           0
        1       0       0           0           0
        0       1       1           1           1   rune soon false
        0       1       1           0           0   rune soon false
        0       1       0           1           1
        0       1       0           0           1
        0       0       1           1           1   usually invalid, but could happen during casting rune growth 
        0       0       1           0           1   usually invalid, but could happen during casting rune growth
        0		0		0           1			1
        0		0	    0           0			1

        switch = 1:                                     terms
        1       0       1           0           1        2
        0       1       1           1           1        2
        0       1       0           1           1        1 12
        0       1       0           0           1        1 12
        0       0       1           1           1        1 11
        0       0       1           0           1        1 11
        0		0		0           1			1        1 11
        0		0	    0           0			1        1 11
        
        Result:          1             11        12                           2
        switch = (not shield and (not rune or not rcd)) or (rcd and shield == not rune and rune == tt)

        Alternatively, rune could be defined as (attack rune growth or defense rune growth) instead of only (attack rune growth)
    ]]--
    elseif (
                not buff_exists[SHIELD_FORM_BUFF] and
                (
                    not buff_exists[ATTACK_RUNE_GROWTH_BUFF] or
                    not cd_done(RUNE_GROWTH)
                )
           ) or
           (
                not not buff_exists[SHIELD_FORM_BUFF] == not not not buff_exists[ATTACK_RUNE_GROWTH_BUFF] and
                not not buff_exists[ATTACK_RUNE_GROWTH_BUFF] == danger() and
                cd_done(RUNE_GROWTH)
           ) then
        CastSpellByName_wrapper(SHIELD_FORM_NAME)
    elseif target_invalid then
        return
    elseif rage >= 20 and
           debuff_exists[HEAVY_BASH_BUFF] and
           stacks_debuff[HEAVY_BASH_BUFF] == 3 and
           durations_debuff[HEAVY_BASH_BUFF] < 2.5 then
        CastSpellByName_wrapper(HEAVY_BASH_NAME)
    -- cooldown satisfied implicitly as 4 stages require at least 20 seconds, but there could be multiple champions adding buffs
    elseif (buff_exists[CHAIN_DRIVE] or buff_exists[CHAIN_DRIVE_WARRIOR]) and
           stacks_debuff[DISASSEMBLING_STAGE] == 4 and
           suit_cd_done(PUNISHERS_DISASSEMBLY) then
        CastSpellByName_wrapper(names[PUNISHERS_DISASSEMBLY])
    -- check cd because sometimes it happens that the full cd is active despite buff
    elseif (buff_exists[CHAIN_DRIVE] or buff_exists[CHAIN_DRIVE_WARRIOR]) and
           cd_done(RUNE_PULSE) then
        CastSpellByName_wrapper(RUNE_PULSE_NAME)
    elseif buff_exists[DEATH_ARRIVES_BUFF] and cd_done(SHADOW_STEP) then
        CastSpellByName_wrapper(names[SHADOW_STEP])
    elseif Skill_cooldown(skill_tab_ids, skill_slots, HEAVY_BASH) == 0 and
           not Get_skill_usable_on_action_bar(skill_action_slots, HEAVY_BASH) and
           rage >= 20 and
           cd_done(FEARLESS_BLOW) then
        CastSpellByName_wrapper(names[FEARLESS_BLOW])
    elseif rage >= 15 and
           target_relative_health < .45 and
           cd_done(FEARLESS_BLOW) then
        CastSpellByName_wrapper(names[FEARLESS_BLOW])
    elseif rage >= 20 and
           ( not debuff_exists[HEAVY_BASH_BUFF] or
             stacks_debuff[HEAVY_BASH_BUFF] < 3 ) then
        CastSpellByName_wrapper(HEAVY_BASH_NAME)
    elseif rage >= 15 and
           cd_done(FEARLESS_BLOW) then
        CastSpellByName_wrapper(names[FEARLESS_BLOW])
    elseif rage >= 25 and
           debuff_exists[ELECTROCUTION_BUFF] and
           cd_done(ENERGY_INFLUX_STRIKE) then
        CastSpellByName_wrapper(names[ENERGY_INFLUX_STRIKE])
    elseif rage >= 45 and -- 45 required to execute combo with energy influx strike
           cd_done(ELECTROCUTION) then
        CastSpellByName_wrapper(names[ELECTROCUTION])
    elseif rage >= 45 then
        CastSpellByName_wrapper(HEAVY_BASH_NAME)
    else
        CastSpellByName_wrapper(ATTACK_NAME)
    end
end

function FalafelDoener.Attack_champion_mage_distance()
    Update_current(
        durations_buff,
        stacks_buff,
        params_buff,
        UnitBuff,
        UnitBuffLeftTime,
        PLAYER
    )

    local rage = UnitMana(PLAYER)
    local target_relative_health = TargetRelativeHealth(TARGET)
    local target_invalid = not target_relative_health

    if not target_invalid and buff_exists[CHAIN_DRIVE] and cd_done(RUNE_PULSE) then
        CastSpellByName_wrapper(RUNE_PULSE_NAME)
    elseif not buff_exists[FORGE_BUFF] or
           durations_buff[FORGE_BUFF] < 3 then
        CastSpellByName_wrapper(FORGE_NAME)
    elseif not buff_exists[ATTACK_RUNE_GROWTH_BUFF] and cd_done(RUNE_GROWTH) then
        CastSpellByName_wrapper(RUNE_GROWTH_NAME)
    elseif target_invalid then
        return
    elseif rage >= 15 and cd_done(FEARLESS_BLOW) then
        CastSpellByName_wrapper(names[FEARLESS_BLOW])
    elseif cd_done(RUNE_PULSE) then
        CastSpellByName_wrapper(RUNE_PULSE_NAME)
    end
end
