local Buffs = {}
_G.FalafelDoener.Buffs = Buffs

Buffs.ids = {}
dofile("interface/addons/falafel_doener/lib/buffs/champion.lua")
dofile("interface/addons/falafel_doener/lib/buffs/rogue.lua")
dofile("interface/addons/falafel_doener/lib/buffs/warlock.lua")
FalafelDoener.Generate_name_mapping(Buffs)

local pairs = pairs
function Buffs.Update_current(
    duration_table,
    stack_table,
    params_table,
    UnitBuff,
    UnitBuffLeftTime,
    target
)
    -- stack_table's keys are a super set, which is why it's used for deleting the tables
    for i, _ in pairs(stack_table) do
        duration_table[i] = nil
        stack_table[i] = nil
        params_table[i] = nil
    end
    for index = 1, 111 do
        local _, _, count, buff_id, params = UnitBuff(target, index)
        if buff_id ~= nil then
            local left_time = UnitBuffLeftTime(target, index)
            duration_table[buff_id] = left_time
            stack_table[buff_id] = count
            params_table[buff_id] = params
        else
            break
        end
    end
end

local print = print
local Update_current = Buffs.Update_current
function Buffs.Print_current(
    duration_table,
    stack_table,
    UnitBuff,
    UnitBuffLeftTime,
    target
)
    Update_current(duration_table, stack_table, {}, UnitBuff, UnitBuffLeftTime, target)
for index, stacks in pairs(stack_table) do
        print("Buff #"..index..": "..stacks.." ("..(duration_table[index] or "infinity")..")")
    end
end