local FalafelDoener = _G.FalafelDoener

local Targets = {}
FalafelDoener.Targets = Targets

Targets.blood_bar_keys = {}
local bar_titles = {}
Targets.blood_bar_keys.bar_titles = bar_titles
local bar_values = {}
Targets.blood_bar_keys.bar_values = bar_values
local target_us = {}
Targets.blood_bar_keys.target_us = target_us
local u_targets = {}
Targets.blood_bar_keys.u_targets = u_targets
local bars = {}
Targets.blood_bar_keys.bars = bars

for i = 1, 40 do
    local blood_bar_name = "OBloodBar"..i
    bars[i]       = blood_bar_name
    bar_titles[i] = blood_bar_name.."Title"
    target_us[i]  = blood_bar_name.."BarTargetU"
    u_targets[i]  = blood_bar_name.."BarUTarget"
    bar_values[i] = blood_bar_name.."BarValue"
end

local _G = _G
local tonumber = tonumber
local GC_GetBloodBar = GC_GetBloodBar
-- Issue: if target has less ]0:1[ hp, function returns false anyways
-- Idea: save bloodbar with your target, target target with 0 hp, check, then switch back 
function Targets.EnemyTargetsMe()
    if not GC_GetBloodBar() then return false end
    for i = 1, 40 do
        local title = _G[bar_titles[i]]
        local r, _g, _b = title:GetColor()
        if r >= 0.9 then
            local targets_me = _G[target_us[i]]:IsVisible()
            if targets_me then
                local hp = _G[bar_values[i]]:GetText()
                local hp_number = tonumber(hp)
                if not (
                    hp_number == false or
                    hp_number == nil
                ) and hp_number > 0 then
                    return true
                end
            end
        end
    end
    return false
end

local target_hps = {}
local target_ids = {}
local function hp_based_sort(a, b)
    return target_hps[a] < target_hps[b]
end

local target_indices = {}

local table = table
local pairs = pairs
local ipairs = ipairs
local UnitExists = UnitExists
local OBB_ChangeTraget = OBB_ChangeTraget
local TARGET_TARGET = "targettarget"
local TargetUnit = TargetUnit
function Targets.TargetFightingEnemyWithLeastHP()
    if not GC_GetBloodBar() then return nil, nil end
    for _ = 1, #target_hps do
        table.remove(target_hps)
        table.remove(target_ids)
        table.remove(target_indices)
    end
    for i = 1, 40 do
        local title = _G[bar_titles[i]]
        local r, _g, _b = title:GetColor()
        if r >= 0.9 then
            local main_bar = _G[bars[i]]
            if main_bar:IsVisible() then
                local hp = _G[bar_values[i]]:GetText()
                local hp_number = tonumber(hp)
                if not (
                    hp_number == false or
                    hp_number == nil
                ) then
                    table.insert(target_hps, hp_number)
                    table.insert(target_ids, main_bar:GetID())
                end
            end
        end
    end
    -- sort inline to avoid closures and allocations
    for index = 1, #target_hps do
        table.insert(target_indices, index)
    end
    table.sort(target_indices, hp_based_sort)
    for _, index in ipairs(target_indices) do
        OBB_ChangeTraget(target_ids[index])
        if UnitExists(TARGET_TARGET) then
            return true
        end
    end
    TargetUnit("")
    return false
    -- Restore previous target?
    -- only use it if no target -> no
    -- allow switching target in fight -> yes (likely don't want)
end


local zero_hp_targets = {}
local nonzero_hp_targets = {}
local visible_targets = {}
local all_targets = { visible_targets, nonzero_hp_targets, zero_hp_targets }

local TARGET = "target"
local next = next
local function find_target_blood_bar(stop_after_visible)
    for i = 1, 40 do
        local is_my_target = _G[u_targets[i]]:IsVisible()
        if is_my_target then
            local hp = tonumber(_G[bar_values[i]]:GetText())
            local id = _G[bars[i]]:GetID()
            if hp ~= 0 then -- 0 might indicate blood bar hasn't been removed and updated since death
                if _G[bars[i]]:IsVisible() then
                    visible_targets[i] = id
                    if stop_after_visible then
                        return
                    end
                else
                    nonzero_hp_targets[i] = id
                end
            else
                zero_hp_targets[i] = id
            end
        end
    end
end

local function determine_current_targeted_blood_bar(old_blood_bar_id, old_blood_bar_index, ignore_non_visible_targets)
    for i, _ in pairs(zero_hp_targets) do
        zero_hp_targets[i] = nil
    end
    for i, _ in pairs(nonzero_hp_targets) do
        nonzero_hp_targets[i] = nil
    end
    for i, _ in pairs(visible_targets) do
        visible_targets[i] = nil
    end

    if not UnitExists(TARGET) then
        return
    elseif not old_blood_bar_id then
        find_target_blood_bar(ignore_non_visible_targets)
    elseif _G[bars[old_blood_bar_index]]:GetID() == old_blood_bar_id and
           _G[u_targets[old_blood_bar_index]]:IsVisible() then
        nonzero_hp_targets[old_blood_bar_index] = old_blood_bar_id
    else
        find_target_blood_bar(ignore_non_visible_targets)
    end
end

local UnitGUID = UnitGUID
local SendChatMessage = SendChatMessage
local UnitName = UnitName
local PLAYER = "player"
local print = print
function Targets.TargetEnemyNotTargetingMe(
    old_blood_bar_id,    -- argument as local variables? other modules?
    old_blood_bar_index, -- argument as local variables? other modules?
    dont_risk_loosing_out_of_screen_target
)
    local current_target = UnitGUID(TARGET)
    if not GC_GetBloodBar() then return nil, nil end
    local id_stored
    for i = 1, 40 do
		local title = _G[bar_titles[i]]
        local r, _g, _b = title:GetColor()
        if r >= 0.9 then
            local targets_me = _G[target_us[i]]:IsVisible()
            if not targets_me then
                local hp = _G[bar_values[i]]:GetText()
                local hp_number = tonumber(hp)
                if not (
                    hp_number == false or
                    hp_number == nil
                ) and hp_number > 0 then -- hp = 0 -> dead
                    local blood_bar_id_to_target = _G[bars[i]]:GetID()
                    if not id_stored then
                        determine_current_targeted_blood_bar(
                            old_blood_bar_id,
                            old_blood_bar_index, dont_risk_loosing_out_of_screen_target
                        )
                        id_stored = true
                        if next(visible_targets) == nil and
                           dont_risk_loosing_out_of_screen_target then
                            print("Target doesn't have visible hp bar, not risking it")
                            return
                        end
                    end
                    OBB_ChangeTraget(blood_bar_id_to_target)
                    if UnitExists(TARGET_TARGET) and
                       not UnitIsUnit(TARGET_TARGET, PLAYER) then -- after Rune draw sometimes the bloodbars don't notice it for a while...
                        SendChatMessage("Stop attacking ".. UnitName(TARGET_TARGET)..", "..title:GetText().." ("..hp_number..")".."!", "SAY")
                        return blood_bar_id_to_target, i
                    end
                end
            end
        end
    end
    if id_stored then
        --invalid bar id results in returning invalid bar id if there is no target
        if current_target == nil then
            TargetUnit("")
            return nil, nil
        end
        for _, targets in ipairs(all_targets) do
            for bar_index, bar_id in pairs(targets) do
                OBB_ChangeTraget(bar_id)
                if UnitGUID(TARGET) == current_target then
                    return bar_id, bar_index
                end
            end
        end
        TargetUnit("")
        return nil, nil
    end
end

local UnitHealth = UnitHealth
local UnitMaxHealth = UnitMaxHealth
local UnitCanAttack = UnitCanAttack
function Targets.TargetRelativeHealth()
    local target_health = UnitHealth(TARGET)
    local target_relative_health = target_health / UnitMaxHealth(TARGET)
    local target_invalid =
        target_health == 0 or
        not UnitCanAttack(PLAYER, TARGET)
    if target_invalid then
        return nil
    else
        return target_relative_health
    end
end

local TargetFightingEnemyWithLeastHP = Targets.TargetFightingEnemyWithLeastHP
local TargetNearestEnemy = TargetNearestEnemy
local tostring = tostring
local should_target_unengaged_target = false
function Targets.AutoTarget()
    if not TargetFightingEnemyWithLeastHP() and
       should_target_unengaged_target then
            TargetNearestEnemy()
    end
end

function Targets.ToggleUnengaged()
    should_target_unengaged_target = not should_target_unengaged_target
    print("should_target_unengaged_target = "..tostring(should_target_unengaged_target))
end