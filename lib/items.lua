local Items = {}
_G.FalafelDoener.Items = Items
local ids = {}
Items.ids = ids

ids.AOTH_VALIANCE_CAPE = 227728
ids.STRENGTH_OF_BATTLE = 203507

FalafelDoener.Generate_name_mapping(Items)

local use_equipment_slots = {}
use_equipment_slots.CAPE = 5

local equip_slots = {}
equip_slots.CAPE = 6


local EquipItem = EquipItem
local AOTH_VALIANCE_CAPE = ids.AOTH_VALIANCE_CAPE
function Items.InitiateUseOfRageCape(goods_item_slots, old_goods_item_slot_of_item_equiped)
    local goods_item_slot = goods_item_slots[AOTH_VALIANCE_CAPE]
    EquipItem(equip_slots.CAPE, goods_item_slot)
    old_goods_item_slot_of_item_equiped[AOTH_VALIANCE_CAPE] = goods_item_slot
end

local GetGoodsItemCooldown = GetGoodsItemCooldown
function Items.GetBagItemCooldown(goods_item_slots, id)
    return GetGoodsItemCooldown(goods_item_slots[id])
end

local UseEquipmentItem = UseEquipmentItem
local GetEquipItemCooldown = GetEquipItemCooldown
function Items.HandleUseOfRageCape(old_goods_item_slot_of_item_equiped)
    local old_goods_item_slot = old_goods_item_slot_of_item_equiped[AOTH_VALIANCE_CAPE]
    if old_goods_item_slot then
        local max_cd, cd = GetEquipItemCooldown(use_equipment_slots.CAPE)
        if cd == 0 then
            -- TODO: check in a few seconds if original state, and switch - if needed repeatedly
            UseEquipmentItem(use_equipment_slots.CAPE)
        -- in case it's unrelieable
        --elseif max_cd ~= 0 then
        --    EquipItem(equip_slots.CAPE, old_goods_item_slot)
        else
            EquipItem(equip_slots.CAPE, old_goods_item_slot)
            old_goods_item_slot_of_item_equiped[AOTH_VALIANCE_CAPE] = nil
        end
        return true
    end
    return false
end

local pairs = pairs
local GetGoodsItemInfo = GetGoodsItemInfo
function Items.UpdateGoodsItemIndices(goods_indices, item_name_to_id)
    for i, _ in pairs(goods_indices) do
        goods_indices[i] = nil
    end

    for id = 51, 60 + 180 do
        local _, name, count = GetGoodsItemInfo(id)
        if count ~= 0 then
            -- alternatively: _, id, _ = ParseHyperlink(GetBagItemLink(bag_index, true))
            local item_id = item_name_to_id[name]
            if item_id ~= nil then
                goods_indices[
                    item_id
                ] = id
            end
        end
    end
end