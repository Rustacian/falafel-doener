local Timer = {}
_G.FalafelDoener.Timer = Timer

local timer_lambdas = {}
Timer.timer_lambdas = timer_lambdas
local timer_delays = {}
Timer.timer_delays = timer_delays
local current_index = 0

Timer.Frame = CreateUIComponent("Frame", "Timer_Frame", "", nil)
local frame = Timer.Frame
_G[frame:GetName()] = nil
frame:Hide()

local next = next
local function update_frame()
	if next(timer_lambdas) == nil then
        frame:Hide()
    end
end

local pairs = pairs
function Timer:OnUpdate(elapsedTime)
	for key, delay in pairs(timer_delays) do
		local new_delay = delay - elapsedTime
		if new_delay <= 0 then
			local next_time = timer_lambdas[key]()
			if next_time then
				timer_delays[key] = next_time
			else
				Timer.Delete(key)
			end
		else
			timer_delays[key] = new_delay
		end
	end
	update_frame()
end

function Timer.do_nothing() end

function Timer.ResetTimer(timer_index, delay, lambda)
	frame:Show()
	timer_delays[timer_index] = delay
	if lambda then
		timer_lambdas[timer_index] = lambda
	end
end

function Timer.ReserveSlot()
	local index = current_index
	current_index  = current_index + 1
	return index
end

function Timer.TimerFinished(timer_index)
	return timer_lambdas[timer_index] == nil
end

function Timer.Delete(timer_index)
	timer_delays[timer_index] = nil
	timer_lambdas[timer_index] = nil
	update_frame()
end

function Timer.Add(delay, lambda)
	frame:Show()
	local index = Timer.ReserveSlot()
	Timer.ResetTimer(index, delay, lambda)
	return index
end

function Timer.Unload()
    frame:SetScripts("OnUpdate", "")
end

frame:SetScripts("OnUpdate", "FalafelDoener.Timer:OnUpdate(elapsedTime)")