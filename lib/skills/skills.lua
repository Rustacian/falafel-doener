local Skills = {}
_G.FalafelDoener.Skills = Skills

Skills.ids = {}
dofile("interface/addons/falafel_doener/lib/skills/general.lua")
dofile("interface/addons/falafel_doener/lib/skills/champion.lua")
dofile("interface/addons/falafel_doener/lib/skills/rogue.lua")
dofile("interface/addons/falafel_doener/lib/skills/warlock.lua")
FalafelDoener.Generate_name_mapping(Skills)

local GetNumSkill = GetNumSkill
local GetSkillDetail = GetSkillDetail
local pairs = pairs
local GetSkillHyperLink = GetSkillHyperLink
local ParseHyperlink = ParseHyperlink
local sub = string.sub
local tonumber = tonumber
function Skills.Update_skill_information(
    tab_ids,
    slots
)
    for i, _ in pairs(tab_ids) do
        tab_ids[i] = nil
        slots[i] = nil
    end
    for tab_id = 1, 4 do
		local slot_count = GetNumSkill(tab_id)
        if slot_count ~= nil then
            for slot = 1, slot_count do
                local name, _, icon, _, _, _, _, _, _ = GetSkillDetail(tab_id, slot);
                local _, id_lvl_str, _ = ParseHyperlink(
                    GetSkillHyperLink(tab_id, slot)
                )
                local id = tonumber(sub(id_lvl_str, 1, 6))
                ---@diagnostic disable-next-line: need-check-nil
                tab_ids[id] = tab_id
                ---@diagnostic disable-next-line: need-check-nil
                slots[id] = slot
            end
        end
	end
end

local GetSkillCooldown = GetSkillCooldown
function Skills.Update_skill_cooldowns(
    tab_ids,
    slots,
    skill_cooldowns
)
    for id, tab_id in pairs(tab_ids) do
        local slot = slots[id]
        local _, cooldown = GetSkillCooldown(tab_id, slot)
        skill_cooldowns[id] = cooldown
    end
end
function Skills.Get_skill_cooldown(tab_ids, slots, id)
    if id == nil then print(id, tab_ids, slots) end
    if not tab_ids[id] then print("id not found: "..Dump(id)) end
    local _, cooldown = GetSkillCooldown(
        tab_ids[id],
        slots[id]
    )
    return cooldown
end


local UnitClassToken = UnitClassToken
local SetSuitSkill_List = SetSuitSkill_List
function Skills.Get_suit_skills_info_for_class()
    local class, _ = UnitClassToken("player")
    local warrior, scout, rogue, mage, priest, knight, warden, druid, warlock, champion, _ = SetSuitSkill_List()

    if     class == "WARRIOR"   then return  1, warrior
    elseif class == "RANGER"    then return  2, scout
    elseif class == "THIEF"     then return  3, rogue
    elseif class == "MAGE"      then return  4, mage
    elseif class == "AUGUR"     then return  5, priest
    elseif class == "KNIGHT"    then return  6, knight
    elseif class == "WARDEN"    then return  7, warden
    elseif class == "DRUID"     then return  8, druid
    elseif class == "HARPSYN"   then return  9, warlock
    elseif class == "PSYRON"    then return 10, champion
    end
end

local function action_slot_contains_skill(icon_path, name)
    return icon_path ~= nil and name == ""
end

local SkillPlateUpdate = SkillPlateUpdate
local GetActionInfo = GetActionInfo
local pairs = pairs
function Skills.Update_suit_skill_action_slots(
    set_skill_textures,
    set_skill_names,
    suit_skill_action_slots,
    skill_name_to_id
)
    for i = 0, 4 do
        set_skill_names[i], set_skill_textures[i] = SkillPlateUpdate(i)
    end

    for slot = 1, 80 do
        local icon_path, name, _, _, _ = GetActionInfo(slot)
        if action_slot_contains_skill(icon_path, name) then
            for index, texture in pairs(set_skill_textures) do
                if texture == icon_path then
                    local id = skill_name_to_id[set_skill_names[index]]
                    -- remove conditional path if setskills for all classes are added
                    if id == nil then
                        print(set_skill_names[index].." ( "..texture.." ) ID missing")
                    else
                        suit_skill_action_slots[id] = slot
                    end
                    break
                end
            end
        end
    end
end

local PickupAction = PickupAction
local GetCursorItemInfo = GetCursorItemInfo
local CursorItemType = CursorItemType
local ACTION = "action"
function Skills.Update_skill_action_slots(
    action_slot_icons,
    action_slot_names,
    skill_action_slots
)
    for slot = 1, 80 do
        local icon_path, name, _, _, _ = GetActionInfo(slot)
        if action_slot_icons[slot] ~= icon_path or
           action_slot_names[slot] ~= name then
            action_slot_icons[slot] = icon_path
            action_slot_names[slot] = name
            PickupAction(slot)
            if CursorItemType() == ACTION then
                local id = GetCursorItemInfo() - 1
                skill_action_slots[id] = slot
            end
            PickupAction(slot)
        end
    end
    return false
end

local GetActionCooldown = GetActionCooldown
function Skills.Update_suit_skill_cooldowns(
    suit_skill_action_slots, 
    skill_cooldowns
)
    for id, slot in pairs(suit_skill_action_slots) do
        local _, cooldown = GetActionCooldown(slot);
        skill_cooldowns[id] = cooldown
    end
end

function Skills.Get_suit_skill_cooldown(
    suit_skill_action_slots,
    id
)
    local _, cooldown = GetActionCooldown(
        suit_skill_action_slots[id]
    );
    return cooldown
end

local GetActionUsable = GetActionUsable
function Skills.Get_skill_usable_on_action_bar(
    skill_action_slots,
    id
)
    return GetActionUsable(
        skill_action_slots[id]
    )
end

local print = print
local CastSpellByName = CastSpellByName
local logging = false
function Skills.CastSpellByName_wrapper(name)
    if logging then
        print(name)
    end
    CastSpellByName(name)
end
function Skills.CastSpellById(id, names)
    local name = names[id]
    if logging then
        print(name)
    end
    CastSpellByName(name)
end
function Skills.ToggleLogging()
   logging = not logging
end