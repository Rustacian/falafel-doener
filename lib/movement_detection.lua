local Movement_detection = {} 
_G.FalafelDoener.Movement_detection = Movement_detection

Movement_detection.X_INDEX = 1
Movement_detection.Y_INDEX = 2
Movement_detection.IS_MOVING_INDEX = 3

local X_INDEX = Movement_detection.X_INDEX
local Y_INDEX = Movement_detection.Y_INDEX
local IS_MOVING_INDEX = Movement_detection.IS_MOVING_INDEX
local GetPlayerWorldMapPos = GetPlayerWorldMapPos
local abs = math.abs
function Movement_detection.Update_movement(variable_table)
    local x, y = GetPlayerWorldMapPos()
    -- make nil to indicate data is not avaialble? 
    -- may be necessary for warlock skills which require movement
    variable_table[IS_MOVING_INDEX] = 
        not (variable_table[X_INDEX] == x) or 
        not (variable_table[Y_INDEX] == y) or
        x == nil or 
        y == nil
    variable_table[X_INDEX] = x
    variable_table[Y_INDEX] = y
end