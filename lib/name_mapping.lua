local FalafelDoener = _G.FalafelDoener

local TEXT = TEXT
local pairs = pairs
function FalafelDoener.Generate_name_mapping(library)
    library.names = {}
    library.ids_by_name = {}
    for _, id in pairs(library.ids) do
        local name = TEXT("Sys" .. id .. "_name")
        library.names[id] = name
        library.ids_by_name[name] = id
    end
end