function FalafelDoener.Reload()
	FalafelDoener.EventHandler.Unload()
	FalafelDoener.Timer.Unload()

	dofile("interface/addons/falafel_doener/runtime_data.lua")

	dofile("interface/addons/falafel_doener/lib/timer.lua")
	dofile("interface/addons/falafel_doener/lib/name_mapping.lua")
	dofile("interface/addons/falafel_doener/lib/skills/skills.lua")
	dofile("interface/addons/falafel_doener/lib/buffs/buffs.lua")
	dofile("interface/addons/falafel_doener/lib/items.lua")
	dofile("interface/addons/falafel_doener/lib/targets.lua")
	dofile("interface/addons/falafel_doener/lib/movement_detection.lua")

	dofile("interface/addons/falafel_doener/event_handler.lua")

	dofile("interface/addons/falafel_doener/champion_rogue.lua")
	dofile("interface/addons/falafel_doener/champion_warrior.lua")
	dofile("interface/addons/falafel_doener/champion_mage.lua")
	dofile("interface/addons/falafel_doener/rogue_warlock.lua")
	dofile("interface/addons/falafel_doener/warlock_rogue.lua")

	dofile("interface/addons/falafel_doener/falafel_doener.lua")
	
	_G.FalafelDoener.EventHandler:LOADING_END() -- refetch global module to avoid replaced module
end

local Print_current = FalafelDoener.Buffs.Print_current
local runtime_data = FalafelDoener.runtime_data
local indices = FalafelDoener.indices
local BuffDuration = indices.BuffDuration
local BuffStack = indices.BuffStack
local UnitBuff = UnitBuff
local UnitBuffLeftTime = UnitBuffLeftTime
FalafelDoener.Print_current_player_buffs = function() Print_current(
    runtime_data[BuffDuration],
    runtime_data[BuffStack],
	UnitBuff,
	UnitBuffLeftTime,
	"player"
) end

local DebuffDuration = indices.DebuffDuration
local DebuffStack = indices.DebuffStack
local UnitDebuff = UnitDebuff
local UnitDebuffLeftTime = UnitDebuffLeftTime
FalafelDoener.Print_current_target_debuffs = function() Print_current(
    runtime_data[DebuffDuration],
    runtime_data[DebuffStack],
    UnitDebuff,
    UnitDebuffLeftTime,
    "target"
) end

FalafelDoener.Print_current_target_buffs = function() Print_current(
    runtime_data[BuffDuration],
    runtime_data[BuffStack],
    UnitDebuff,
    UnitDebuffLeftTime,
    "target"
) end

function Dump(data, nest)
	-- 再帰呼出しのネストを制御
	if(nest == nil) then
		nest = 0;
	else
		nest = nest + 1;
	end
	-- ネストの深さをチェック
	if(nest ~= nil and nest > 1000) then
		error("Error: Stack overflow. Data:[" .. tostring(data) .. "], Index:[" .. tostring(nest) .. "]");
	end

	local line = "";
	if(type(data) == "string") then
		if(data == "") then
			line = "<blank>";
		else
			line = tostring(data);
		end
	elseif(type(data) == "table") then
		-- 配列
		local meta = getmetatable(data);
		if(meta ~= nil and meta.__tostring ~= nil) then
			line = tostring(data);
		elseif(next(data) == nil) then
			line = "<empty>";
		else
			for key, value in pairs(data) do
				if(line ~= "") then
					line = line .. ", ";
				end

				if(type(key) == "string" and key:sub(1, 2) == "__") then
					-- メタテーブルへ設定する情報と思われるものは、循環参照するので、詳細は表示しない。
					line = line .. "[" .. key .. "]:" .. type(value) .. ""
				else
					line = line .. "[" .. Dump(key, nest) .. "]:" .. Dump(value, nest) .. "";	-- 再帰呼出し
				end
			end
		end
	else
		-- nil、数値、ブーリアン型、function
		line = tostring(data);
	end

	return line;
end;


