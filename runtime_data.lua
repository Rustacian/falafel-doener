local FalafelDoener = {}
_G.FalafelDoener = FalafelDoener

local d = {}
FalafelDoener.runtime_data = d
local is = {}
FalafelDoener.indices = is

local pos = 0
pos = pos + 1   d[pos] = {}     is.BuffDuration = pos
pos = pos + 1   d[pos] = {}     is.BuffStack = pos
                                is.BuffExists = pos         -- reuse data of buff stack
pos = pos + 1   d[pos] = {}     is.BuffParams = pos
pos = pos + 1   d[pos] = {}     is.BuffIndices = pos

pos = pos + 1   d[pos] = {}     is.DebuffDuration = pos
pos = pos + 1   d[pos] = {}     is.DebuffStack = pos
                                is.DebuffExists = pos       -- reuse data of debuff stack
pos = pos + 1   d[pos] = {}     is.DebuffParams = pos
pos = pos + 1   d[pos] = {}     is.DebuffIndices = pos

pos = pos + 1   d[pos] = {}     is.SkillTabIDs = pos
pos = pos + 1   d[pos] = {}     is.SkillSlots = pos
pos = pos + 1   d[pos] = {}     is.ACTION_SLOT_ICONS = pos
pos = pos + 1   d[pos] = {}     is.ACTION_SLOT_NAMES = pos
pos = pos + 1   d[pos] = {}     is.SkillCooldowns = pos
pos = pos + 1   d[pos] = {}     is.SKILL_ACTION_SLOTS = pos

pos = pos + 1   d[pos] = {}     is.ClassIndex = pos
pos = pos + 1   d[pos] = {}     is.SuitSkillCount = pos

pos = pos + 1   d[pos] = {}     is.SetSkillTextures = pos
pos = pos + 1   d[pos] = {}     is.SetSkillNames = pos
pos = pos + 1   d[pos] = {}     is.SUIT_SKILL_ACTION_SLOTS = pos

pos = pos + 1   d[pos] = {}     is.GOODS_INDICES = pos
pos = pos + 1   d[pos] = {}     is.ITEM_EQUIPED_OLD_GOODS_INDICES = pos

pos = pos + 1   d[pos] = {0, 0, False}     is.MOVEMENT_DETECTION = pos