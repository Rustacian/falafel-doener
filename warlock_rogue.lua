local FalafelDoener = _G.FalafelDoener
local runtime_data = FalafelDoener.runtime_data
local indices = FalafelDoener.indices
local Buffs = FalafelDoener.Buffs
local Skills = FalafelDoener.Skills
local Targets = FalafelDoener.Targets
local Movement_detection = FalafelDoener.Movement_detection
local s_ids = Skills.ids
local buff_ids = Buffs.ids
local names = Skills.names

local durations_buff = runtime_data[indices.BuffDuration]
local stacks_buff = runtime_data[indices.BuffStack]
local buff_exists = runtime_data[indices.BuffExists]
local debuff_exists = runtime_data[indices.DebuffExists]
local params_buff =  runtime_data[indices.BuffParams]
local durations_debuff =  runtime_data[indices.DebuffDuration]
local stacks_debuff =  runtime_data[indices.DebuffStack]
local params_debuff =  runtime_data[indices.DebuffParams]
local skill_tab_ids = runtime_data[indices.SkillTabIDs]
local skill_slots = runtime_data[indices.SkillSlots]
local set_skill_action_slots = runtime_data[indices.SUIT_SKILL_ACTION_SLOTS]
local skill_action_slots = runtime_data[indices.SKILL_ACTION_SLOTS]
local movement_data = runtime_data[indices.MOVEMENT_DETECTION]

local PLAYER = "player"
local TARGET = "target"

local UnitMana = UnitMana
local UnitSkill = UnitSkill
local UnitHealth = UnitHealth
local UnitMaxHealth = UnitMaxHealth
local UnitBuff = UnitBuff
local UnitBuffLeftTime = UnitBuffLeftTime
local UnitDebuff = UnitDebuff
local UnitDebuffLeftTime = UnitDebuffLeftTime
local print = print
local GetPlayerCombatState = GetPlayerCombatState
local UnitCastingTime = UnitCastingTime
local GetSoulPoint = GetSoulPoint

local CastSpellById = Skills.CastSpellById
local Update_current = Buffs.Update_current
local Skill_cooldown = Skills.Get_skill_cooldown
local Suit_skill_cooldown = Skills.Get_suit_skill_cooldown
local Get_skill_usable_on_action_bar = Skills.Get_skill_usable_on_action_bar
local AutoTarget = Targets.AutoTarget
local TargetRelativeHealth = Targets.TargetRelativeHealth

local THROW                  =s_ids.THROW_WARLOCK
local SHADOWSTAB             =s_ids.SHADOWSTAB_WARLOCK
local WARP_CHARGE            =s_ids.WARP_CHARGE
local WEAKENING_WEAVE_CURSE  =s_ids.WEAKENING_WEAVE_CURSE
local HEART_COLLECTION_STRIKE=s_ids.HEART_COLLECTION_STRIKE
local WILLPOWER_BLADE        =s_ids.WILLPOWER_BLADE
local PERCEPTION_EXTRACTION  =s_ids.PERCEPTION_EXTRACTION
local SOUL_PAIN              =s_ids.SOUL_PAIN
local SOUL_POISENED_FANG     =s_ids.SOUL_POISENED_FANG
local CRIME_AND_PUNISHMENT   =s_ids.CRIME_AND_PUNISHMENT
local SEVERED_CONSCIOUSNESS  =s_ids.SEVERED_CONSCIOUSNESS
local PUZZLEMENT             =s_ids.PUZZLEMENT
local PSYCHIC_ARROWS         =s_ids.PSYCHIC_ARROWS
local SUBLIMATION_WEAVE_CURSE=s_ids.SUBLIMATION_WEAVE_CURSE
local SURGE_OF_MALICE        =s_ids.SURGE_OF_MALICE
local SOUL_TRAUMA            =s_ids.SOUL_TRAUMA

local WARP_CHARGE_BUFF             = buff_ids.WARP_CHARGE
local WEAKENED                     = buff_ids.WEAKENED
local SOUL_PAIN_BUFF               = buff_ids.SOUL_PAIN
local PERCEPTION_EXTRACTION_BUFF   = buff_ids.PERCEPTION_EXTRACTION
local SOUL_POISENED_FANG_BUFF      = buff_ids.SOUL_POISENED_FANG
local SUBLIMATION_WEAVE_CURSE_BUFF = buff_ids.SUBLIMATION_WEAVE_CURSE

local TOLERANCE = 0.41
local function cd_done(id)
    return Skill_cooldown(skill_tab_ids, skill_slots, id) < TOLERANCE
end
local function cd(id)
    return Skill_cooldown(skill_tab_ids, skill_slots, id)
end
local function suit_cd_done(id)
    return Suit_skill_cooldown(set_skill_action_slots, id) < TOLERANCE
end

local throw_enabled = true
function FalafelDoener.ToggleThrow()
    throw_enabled = not throw_enabled
end
-- TODO? recast perception extraction after crime and punishment to incease its damage?
-- TODO: other willpower skills
-- TODO: during willpower, use energy less -> faster psi recovery
function FalafelDoener.Attack_warlock_rogue()
    Update_current(
        durations_buff,
        stacks_buff,
        params_buff,
        UnitBuff,
        UnitBuffLeftTime,
        PLAYER
    )
    Update_current(
        durations_debuff,
        stacks_debuff,
        params_debuff,
        UnitDebuff,
        UnitDebuffLeftTime,
        TARGET
    )

    local focus = UnitMana(PLAYER)
    local energy = UnitSkill(PLAYER)
    local target_relative_health = TargetRelativeHealth()
    local target_invalid = not target_relative_health
    local in_combat = GetPlayerCombatState()
    local is_standing = not movement_data[Movement_detection.IS_MOVING_INDEX]
    local psi, willpower_mode_active = GetSoulPoint()
    willpower_mode_active = willpower_mode_active == 1

    if target_invalid then
        AutoTarget()
        target_relative_health = TargetRelativeHealth()
        target_invalid = not target_relative_health
    end

    local target_is_boss = UnitSex(TARGET) > 2
    local cast_name, cast_duration, cast_time_done = UnitCastingTime(PLAYER)
    local cast_done = 
        cast_name == nil or 
        ( (cast_duration - cast_time_done) < .1 ) -- move to dedicated function? duplicate in rogue_warlock.lua + future casters
    if target_invalid and 
       focus >= 50 and
       cast_done and
       (
            not buff_exists[SUBLIMATION_WEAVE_CURSE_BUFF] or
            durations_buff[SUBLIMATION_WEAVE_CURSE_BUFF] < 615
       ) then 
        CastSpellById(SUBLIMATION_WEAVE_CURSE, names)
    elseif target_invalid or 
           not cast_done then
        return
    elseif throw_enabled and 
           not cd_done(SHADOWSTAB) then -- prevent cast interuption
        -- global cooldown active -> run towards target for THROW?
        CastSpellById(THROW, names)
    elseif cd_done(THROW) and
           throw_enabled and
            not (
                not Get_skill_usable_on_action_bar(skill_action_slots, THROW) and
                (
                    (focus >= 15 and Get_skill_usable_on_action_bar(skill_action_slots, SOUL_PAIN)) or
                    (energy >= 20 and Get_skill_usable_on_action_bar(skill_action_slots, SHADOWSTAB))
                )
            ) then 
        CastSpellById(THROW, names)
    ---------------------
    -- Willpower Blade --
    ---------------------
    elseif willpower_mode_active and 
           target_is_boss and
           psi >= 2 and 
           ( debuff_exists[SOUL_PAIN_BUFF] or not is_standing ) and
           debuff_exists[PERCEPTION_EXTRACTION_BUFF] and
           debuff_exists[SOUL_POISENED_FANG_BUFF] and
           cd_done(CRIME_AND_PUNISHMENT) and
           (
                (
                    debuff_exists[SOUL_PAIN_BUFF] and 
                    durations_debuff[SOUL_PAIN_BUFF] < 3
                ) or
                durations_debuff[SOUL_POISENED_FANG_BUFF] < 3 or 
                cd(WILLPOWER_BLADE) < 3
           ) then 
        CastSpellById(CRIME_AND_PUNISHMENT, names)
    elseif willpower_mode_active and 
           (
                psi >= 4 or (
                    (not target_is_boss) and
                    psi >= 2
                )
                -- or 
                -- make two executions of severed consciousness more likely at the cost of likely dropped buffs
                -- ( // allow renewal of buffs at least once via 
                --     psi >= 2 and 
                --     not cd_done(CRIME_AND_PUNISHMENT) and
                --     // implies either recent added buffs or Crime and Punishment cast
                --     durations_debuff[SOUL_POISENED_FANG_BUFF] > 15 and
                --     durations_debuff[SOUL_POISENED_FANG_BUFF] > 15 and
                --     durations_debuff[SOUL_POISENED_FANG_BUFF] > 15 and 
                -- )
           ) then
        CastSpellById(SEVERED_CONSCIOUSNESS, names)
    elseif willpower_mode_active and 
           target_is_boss and
           focus >= 15 and
           psi >= 2 and
           is_standing and
           not debuff_exists[SOUL_PAIN_BUFF] then
        CastSpellById(SOUL_PAIN, names)
    elseif willpower_mode_active and 
           psi >= 2 and
           focus >= 15 and
           not debuff_exists[PERCEPTION_EXTRACTION_BUFF] then
        CastSpellById(PERCEPTION_EXTRACTION, names)
    elseif willpower_mode_active and 
           target_is_boss and
           energy >= 25 and
           psi >= 2 and
           not debuff_exists[SOUL_POISENED_FANG_BUFF] then
        CastSpellById(SOUL_POISENED_FANG, names)
    elseif psi >= 6 and
           not willpower_mode_active and
           cd_done(WILLPOWER_BLADE) then 
        CastSpellById(WILLPOWER_BLADE, names)
    ---------------------
    ---------------------
    elseif focus >= 50 and
           (
                not buff_exists[SUBLIMATION_WEAVE_CURSE_BUFF] or
                durations_buff[SUBLIMATION_WEAVE_CURSE_BUFF] < 3
           ) then
        CastSpellById(SUBLIMATION_WEAVE_CURSE, names)
    elseif focus >= 30 and
           debuff_exists[WEAKENED] and
           cd_done(WARP_CHARGE) then
        CastSpellById(WARP_CHARGE, names)
    -- not enough focus for warp charge
    elseif debuff_exists[WEAKENED] and
           cd_done(WARP_CHARGE) and 
           cd_done(HEART_COLLECTION_STRIKE) then
        CastSpellById(HEART_COLLECTION_STRIKE, names)
    elseif focus >= 20 and
           is_standing and
            (
                not debuff_exists[WEAKENED] or
                durations_debuff[WEAKENED] < 2
            ) and
           cd_done(WEAKENING_WEAVE_CURSE) then
        CastSpellById(WEAKENING_WEAVE_CURSE, names)
    -- not enough focus for weakening weave curse
    elseif not debuff_exists[WEAKENED] and 
           cd_done(WEAKENING_WEAVE_CURSE) and 
           cd_done(HEART_COLLECTION_STRIKE) then
        CastSpellById(HEART_COLLECTION_STRIKE, names)
    -- still must be able to add Soul Poisened Fang
    elseif energy >= 65 or (
                energy >= 20 and
                not willpower_mode_active
                -- check if focus almost full and instants available
          ) then 
        CastSpellById(SHADOWSTAB, names)
    elseif focus >= 15 and 
           target_is_boss and
           is_standing and
            (
                not debuff_exists[SOUL_PAIN_BUFF] or
                durations_debuff[SOUL_PAIN_BUFF] < 3
            ) then
        CastSpellById(SOUL_PAIN, names)
    elseif focus >= 15 and 
            (
                not debuff_exists[PERCEPTION_EXTRACTION_BUFF] or
                durations_debuff[PERCEPTION_EXTRACTION_BUFF] < 3
           ) then
        CastSpellById(PERCEPTION_EXTRACTION, names)
    elseif energy >= 25 and 
           target_is_boss and
            (
                not debuff_exists[SOUL_POISENED_FANG_BUFF] or 
                durations_debuff[SOUL_POISENED_FANG_BUFF] < 3
            ) then
        CastSpellById(SOUL_POISENED_FANG, names)
    elseif cd_done(HEART_COLLECTION_STRIKE) then
        CastSpellById(HEART_COLLECTION_STRIKE, names)
    elseif focus >= 20 and -- todo: prioritize psi generation over warp charge if warp charge buff is active
           not willpower_mode_active and 
           is_standing and
           cd_done(PUZZLEMENT) then
        CastSpellById(PUZZLEMENT, names)
    elseif focus >= 20 and
           is_standing then
        CastSpellById(PSYCHIC_ARROWS, names)
    -------------------------------
    -- Fallbacks during movement --
    -------------------------------
    elseif focus >= 30 and 
           cd_done(WARP_CHARGE) then
        CastSpellById(WARP_CHARGE, names)
    elseif cd_done(HEART_COLLECTION_STRIKE) then
        CastSpellById(HEART_COLLECTION_STRIKE, names)
    elseif energy >= 20 then 
        CastSpellById(SHADOWSTAB, names)
    elseif focus >= 20 and 
           cd_done(SURGE_OF_MALICE) then
        CastSpellById(SURGE_OF_MALICE, names)
    elseif focus >= 25 and 
           cd_done(SOUL_TRAUMA) then
        CastSpellById(SOUL_TRAUMA, names)
    -------------------------------
    -------------------------------
    end
end