local FalafelDoener = _G.FalafelDoener
local runtime_data = FalafelDoener.runtime_data
local indices = FalafelDoener.indices
local Buffs = FalafelDoener.Buffs
local Skills = FalafelDoener.Skills
local Items = FalafelDoener.Items
local Targets = FalafelDoener.Targets
local skill_ids = Skills.ids
local buff_ids = Buffs.ids
local item_ids = Items.ids
local skill_names = Skills.names
local item_names = Items.names

local durations_buff = runtime_data[indices.BuffDuration]
local stacks_buff = runtime_data[indices.BuffStack]
local buff_exists = runtime_data[indices.BuffExists]
local debuff_exists = runtime_data[indices.DebuffExists]
local params_buff =  runtime_data[indices.BuffParams]
local durations_debuff =  runtime_data[indices.DebuffDuration]
local stacks_debuff =  runtime_data[indices.DebuffStack]
local params_debuff =  runtime_data[indices.DebuffParams]
local skill_tab_ids = runtime_data[indices.SkillTabIDs]
local skill_slots = runtime_data[indices.SkillSlots]
local skill_action_slots = runtime_data[indices.SKILL_ACTION_SLOTS]
local set_skill_action_slots = runtime_data[indices.SUIT_SKILL_ACTION_SLOTS]
local goods_indices = runtime_data[indices.GOODS_INDICES]
local old_goods_indices = runtime_data[indices.ITEM_EQUIPED_OLD_GOODS_INDICES]

local UnitMana = UnitMana
local UnitSkill = UnitSkill
local UnitHealth = UnitHealth
local UnitMaxHealth = UnitMaxHealth
local UnitBuff = UnitBuff
local UnitBuffLeftTime = UnitBuffLeftTime
local UnitDebuff = UnitDebuff
local UnitDebuffLeftTime = UnitDebuffLeftTime
local CastSpellByName = CastSpellByName
local print = print
local UnitName = UnitName
local pairs = pairs
local GetPlayerCombatState = GetPlayerCombatState

local CastSpellByName_wrapper = Skills.CastSpellByName_wrapper
local Update_current = Buffs.Update_current
local Skill_cooldown = Skills.Get_skill_cooldown
local Suit_skill_cooldown = Skills.Get_suit_skill_cooldown
local Item_skill_cooldown = Items.GetBagItemCooldown
local Skill_usable_on_action_bar = Skills.Get_skill_usable_on_action_bar
local TargetEnemyNotTargetingMe = Targets.TargetEnemyNotTargetingMe

local FORGE_BUFF = buff_ids.FORGE_BUFF_WARRIOR
local FORGE = skill_ids.FORGE_WARRIOR
local RUNE_DRAW = skill_ids.RUNE_DRAW
local AGITATED_WHIRLPOOL = skill_ids.AGITATED_WHIRLPOOL
local HEAVY_BASH = skill_ids.HEAVY_BASH
local RUNIC_CHARGE = skill_ids.RUNIC_CHARGE
local ENRAGED = skill_ids.ENRAGED
local RUNE_ENERGY_INFLUX = skill_ids.RUNE_ENERGY_INFLUX
local ELECTROCUTION = skill_ids.ELECTROCUTION
local ENERGY_INFLUX_STRIKE = skill_ids.ENERGY_INFLUX_STRIKE
local FEARLESS_BLOW = skill_ids.FEARLESS_BLOW
local RUNE_GROWTH = skill_ids.RUNE_GROWTH

local CHAIN_DRIVE = buff_ids.CHAIN_DRIVE_WARRIOR
local HEAVY_BASH_BUFF = buff_ids.HEAVY_BASH_BUFF
local ELECTROCUTION_BUFF = buff_ids.ELECTROCUTION_BUFF
local RUNE_OVERLOAD_BUFF = buff_ids.RUNE_OVERLOAD
local RUNE_OF_PROTECTION_EQUIPMENT = buff_ids.RUNE_OF_PROTECTION_EQUIPMENT

local AOTH_VALIANCE_CAPE = item_ids.AOTH_VALIANCE_CAPE
local STRENGTH_OF_BATTLE = item_ids.STRENGTH_OF_BATTLE

local FORGE_NAME = skill_names[FORGE]
local AGITATED_WHIRLPOOL_NAME = skill_names[AGITATED_WHIRLPOOL]
local RUNE_DRAW_NAME = skill_names[RUNE_DRAW]
local RUNIC_CHARGE_NAME = skill_names[RUNIC_CHARGE]
local ENRAGED_NAME = skill_names[ENRAGED]
local HEAVY_BASH_NAME = skill_names[HEAVY_BASH]
local ELECTROCUTION_NAME = skill_names[ELECTROCUTION]
local ENERGY_INFLUX_STRIKE_NAME = skill_names[ENERGY_INFLUX_STRIKE]
local FEARLESS_BLOW_NAME = skill_names[FEARLESS_BLOW]
local RUNE_GROWTH_NAME = skill_names[RUNE_GROWTH]
local RUNE_PULSE_NAME = skill_names[skill_ids.RUNE_PULSE]
local ATTACK_NAME = skill_names[skill_ids.ATTACK]
local RUNE_ENERGY_INFLUX_NAME = skill_names[skill_ids.RUNE_ENERGY_INFLUX]
local SLASH_NAME = skill_names[skill_ids.SLASH]

local STRENGTH_OF_BATTLE_MAME = item_names[STRENGTH_OF_BATTLE]

local function cd(id)
    Skill_cooldown(skill_tab_ids, skill_slots, id)
end
local tolerance = 0.41
local function cd_done(id)
    return Skill_cooldown(skill_tab_ids, skill_slots, id) < tolerance
end
local function suit_cd_done(id)
    return Suit_skill_cooldown(set_skill_action_slots, id) < tolerance
end
local function item_cd_done(id)
    return Item_skill_cooldown(goods_indices, id)
end
local function skill_usable(id)
    Skill_usable_on_action_bar(skill_action_slots, id)
end


local UseItemByName = UseItemByName
local function acquire_strength_at_any_cost()
    -- Runic charge has no global cooldown -> first
    if suit_cd_done(RUNIC_CHARGE) then
        CastSpellByName_wrapper(RUNIC_CHARGE_NAME)
    elseif cd_done(ENRAGED) then
        CastSpellByName_wrapper(ENRAGED_NAME)
    elseif item_cd_done(AOTH_VALIANCE_CAPE) then
        Items.InitiateUseOfRageCape(goods_indices, old_goods_indices)
    elseif item_cd_done(STRENGTH_OF_BATTLE) then
        UseItemByName(STRENGTH_OF_BATTLE_MAME)
    else
        CastSpellByName_wrapper(ATTACK_NAME)
        return
    end
end

local PLAYER = "player"
local function aggro_information()
    local TargetList = TargetList
    local player_name_list = TargetList["PLAYER_NAME"]
    local player_hate_list = TargetList["PLAYER_HATE"]

    local self_player_name = UnitName(PLAYER)
    local self_player_hate = nil
    local max_not_player_hate = 0
    for index, name in pairs(player_name_list) do
        local hate = player_hate_list[index]
        if name == self_player_name then
            self_player_hate = hate
        elseif max_not_player_hate < hate then
            max_not_player_hate = hate
        end
        return self_player_hate, max_not_player_hate
    end
end

local TARGET = "target"
local TARGET_TARGET = "targettarget"
local UnitIsUnit = UnitIsUnit
local UnitSex = UnitSex
local UnitExists = UnitExists
function FalafelDoener.Attack_champion_warrior_tank()
    if Items.HandleUseOfRageCape(old_goods_indices) then
        return true
    end

    local in_combat = GetPlayerCombatState()
    local target_targets_me = UnitIsUnit(PLAYER, TARGET_TARGET)
    if (target_targets_me or not UnitExists(TARGET_TARGET)) and
       UnitSex(TARGET) <= 2 then
        TargetEnemyNotTargetingMe()
    end

    Update_current(
        durations_buff,
        stacks_buff,
        params_buff,
        UnitBuff,
        UnitBuffLeftTime,
        PLAYER
    )
    Update_current(
        durations_debuff,
        stacks_debuff,
        params_debuff,
        UnitDebuff,
        UnitDebuffLeftTime,
        TARGET
    )

    local rage = UnitMana(PLAYER)
    local player_hate, max_non_player_hate = aggro_information()

    local aggro_problem =
        not target_targets_me and (
            player_hate == nil or
            max_non_player_hate >= player_hate
        )

    if not cd_done(FORGE) and in_combat then -- global cooldown active
        CastSpellByName_wrapper(ATTACK_NAME)
        return
    elseif not buff_exists[FORGE_BUFF] or
           durations_buff[FORGE_BUFF] < 3 then
        CastSpellByName_wrapper(FORGE_NAME)
    elseif UnitHealth(TARGET) == 0 then
        return
    elseif not in_combat then
        CastSpellByName_wrapper(RUNE_PULSE_NAME)
    elseif aggro_problem and
           cd_done(RUNE_DRAW) then
        CastSpellByName_wrapper(RUNE_DRAW_NAME)
    -- --
    -- cast agitated whirl pool and add rage in case distance can't be checked
    elseif aggro_problem and
           rage >= 20 and
           cd_done(AGITATED_WHIRLPOOL) and -- perhaps store condition as well, because of repeated calls
           cd(HEAVY_BASH) == 0 and
           skill_usable(HEAVY_BASH) then
        CastSpellByName_wrapper(AGITATED_WHIRLPOOL_NAME)
        print("Agitated Whirlpool")
    elseif aggro_problem and
           rage < 20 and
           cd_done(AGITATED_WHIRLPOOL) then -- conditions: want enough rage to test distance low enough for whirlpool 
        acquire_strength_at_any_cost()
    -- --
    -- end agitated whirlpool checks
    elseif rage > 10 and not buff_exists[RUNE_OVERLOAD_BUFF] and
           cd_done(RUNE_ENERGY_INFLUX) then
        CastSpellByName_wrapper(RUNE_ENERGY_INFLUX_NAME)
    elseif buff_exists[CHAIN_DRIVE] then
        CastSpellByName_wrapper(RUNE_PULSE_NAME)
    -- todo: cast shock strik if slash is usable
    elseif rage >= 90 and
           not buff_exists[RUNE_OVERLOAD_BUFF] and
           (
                not debuff_exists[HEAVY_BASH_BUFF] or
                stacks_debuff[HEAVY_BASH_BUFF] < 3 or
                durations_debuff[HEAVY_BASH_BUFF] < 3
           )then
        CastSpellByName_wrapper(HEAVY_BASH_NAME)
    elseif not buff_exists[RUNE_OF_PROTECTION_EQUIPMENT] and
           cd_done(RUNE_GROWTH) then
        CastSpellByName(RUNE_GROWTH_NAME)
    elseif rage >= 15 and
           cd_done(FEARLESS_BLOW) then
        CastSpellByName_wrapper(skill_names[FEARLESS_BLOW])
    -- save up rage if Rune Overload active
    elseif buff_exists[RUNE_OVERLOAD_BUFF] then
        CastSpellByName_wrapper(ATTACK_NAME)
    elseif rage >= 25 and
           debuff_exists[ELECTROCUTION_BUFF] and
           cd_done(ENERGY_INFLUX_STRIKE) then
        CastSpellByName_wrapper(skill_names[ENERGY_INFLUX_STRIKE])
    -- todo arc strike
    elseif rage >= 20 and cd_done(ELECTROCUTION) then
        CastSpellByName_wrapper(skill_names[ELECTROCUTION])
    -- 25 + 25 for potential next Energy Influx Strike
    elseif rage >= 50 then
        CastSpellByName_wrapper(SLASH_NAME)
    else
        CastSpellByName_wrapper(ATTACK_NAME)
    end
end