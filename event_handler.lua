local falafel_doener = _G.FalafelDoener
local runtime_data = falafel_doener.runtime_data
local indices = falafel_doener.indices

local Skills = falafel_doener.Skills
local Items = falafel_doener.Items

local skill_name_to_id = Skills.ids_by_name
local item_name_to_id = Items.ids_by_name

local Timer = falafel_doener.Timer
local Movement_detection = falafel_doener.Movement_detection

local skill_tab_ids = runtime_data[indices.SkillTabIDs]
local skill_slots = runtime_data[indices.SkillSlots]
local action_slot_icons = runtime_data[indices.ACTION_SLOT_ICONS]
local action_slot_names = runtime_data[indices.ACTION_SLOT_NAMES]
local set_skill_textures = runtime_data[indices.SetSkillTextures]
local set_skill_names = runtime_data[indices.SetSkillNames]
local suit_skill_action_slots = runtime_data[indices.SUIT_SKILL_ACTION_SLOTS]
local skill_action_slots = runtime_data[indices.SKILL_ACTION_SLOTS]
local goods_indices = runtime_data[indices.GOODS_INDICES]
local movement_detection_table = runtime_data[indices.MOVEMENT_DETECTION]


local Update_skill_information = Skills.Update_skill_information
local Update_skill_action_slots = Skills.Update_skill_action_slots
local Update_timers = Timer.Update_timers
local Add_timer = Timer.Add
local Timer_in_use = Timer.Timer_in_use
local Update_movement = Movement_detection.Update_movement

local EventHandler = {}
falafel_doener.EventHandler = EventHandler

EventHandler.Frame = CreateUIComponent("Frame", "EventHandler_Frame", "", nil)
local frame = EventHandler.Frame
_G[frame:GetName()] = nil
frame:Hide()

function EventHandler:OnEvent(event, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)
    self[event](self, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)
end

local function movement_detection_handler_wrapper()
    Update_movement(movement_detection_table)
    return 0.05
end
local UnitClassToken = UnitClassToken
local movement_detection_timer_index = Timer.ReserveSlot()
function EventHandler:update_registered_events()
    local class, _ = UnitClassToken("player")
    if class == "PSYRON" or class == "THIEF" or class == "HARPSYN" then
        print("Activating Falafel-Döner...")
        frame:RegisterEvent("SUIT_SKILL_PLATE_UPDATE")
        frame:RegisterEvent("CURSOR_ITEM_UPDATE")
        frame:RegisterEvent("BAG_ITEM_UPDATE")
        
        Update_skill_information(skill_tab_ids, skill_slots)
        self:CURSOR_ITEM_UPDATE()
        self:SUIT_SKILL_PLATE_UPDATE(nil)
        self:BAG_ITEM_UPDATE(nil)
    elseif class ~= nil then
        print("Class "..class.." isn't supported yet.")
        print("Deactivating Falafel-Döner...")
        frame:UnregisterEvent("SUIT_SKILL_PLATE_UPDATE")
        frame:UnregisterEvent("CURSOR_ITEM_UPDATE")
        frame:UnregisterEvent("BAG_ITEM_UPDATE")
    end

    if class == "HARPSYN" or class == "THIEF" then
        Timer.ResetTimer(movement_detection_timer_index, 0, movement_detection_handler_wrapper)
    elseif class ~= nil then
        Timer.Delete(movement_detection_timer_index)
    end
end

local UpdateGoodsItemIndices = Items.UpdateGoodsItemIndices

local function bag_update_handler()
    UpdateGoodsItemIndices(goods_indices, item_name_to_id)
end
local bag_update_rate_limiter_index = Timer.ReserveSlot()
local bag_update_debounce_index = Timer.ReserveSlot()
function EventHandler:BAG_ITEM_UPDATE(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)
    if Timer.TimerFinished(bag_update_rate_limiter_index) then
        bag_update_handler()
        Timer.ResetTimer(bag_update_rate_limiter_index, 1, Timer.do_nothing)
        Timer.Delete(bag_update_debounce_index)
        return
    end
    Timer.ResetTimer(bag_update_debounce_index, 1, bag_update_handler)
end

function EventHandler:EXCHANGECLASS_SUCCESS(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)
    Add_timer(1.11, function()
        Update_skill_information(skill_tab_ids, skill_slots)
        EventHandler:update_registered_events()
    end)
end

local Update_suit_skill_action_slots = Skills.Update_suit_skill_action_slots

local block_new_curser_update_events = false -- avoid nested event handling
local function actionbar_slot_changed_handler()
    Update_suit_skill_action_slots(
        set_skill_textures,
        set_skill_names,
        suit_skill_action_slots,
        skill_name_to_id
    )
    block_new_curser_update_events = true
    Update_skill_action_slots(
        action_slot_icons,
        action_slot_names,
        skill_action_slots
    )
    block_new_curser_update_events = false
end
local action_bar_changed_rate_limiter_index = Timer.ReserveSlot()
local action_bar_changed_debounce_index = Timer.ReserveSlot()

function EventHandler:SUIT_SKILL_PLATE_UPDATE(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)
    Update_suit_skill_action_slots(
       set_skill_textures,
       set_skill_names,
       suit_skill_action_slots,
       skill_name_to_id
   )
end

function EventHandler:RIDE_INVITE_REQUEST()
    AcceptRideMount()
    StaticPopup_Hide("RIDE_INVITE")
end

function EventHandler:LOADING_END(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)
    -- class information isn't available immediately after VARIABLES_LOADED
    EventHandler:update_registered_events()
    EventHandler.Frame:UnregisterEvent("LOADING_END")
end

function EventHandler:VARIABLES_LOADED(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)
    print("Falafel-Döner loaded")
    print("Call Attack_primaryClass_secondaryClass_dd_demo() to attack")
end

local CursorItemType = CursorItemType
function EventHandler:CURSOR_ITEM_UPDATE()
    if block_new_curser_update_events or CursorItemType() then
        return
    end
    if Timer.TimerFinished(action_bar_changed_rate_limiter_index) then
        actionbar_slot_changed_handler()
        Timer.ResetTimer(action_bar_changed_rate_limiter_index, 1, Timer.do_nothing)
        Timer.Delete(action_bar_changed_debounce_index)
        return
    end
    Timer.ResetTimer(action_bar_changed_debounce_index, 1, actionbar_slot_changed_handler)
end

function EventHandler:COMBATMETER_DAMAGE(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)
    if _source == UnitName("player") and _target == UnitName("target") then
       print(_skill..": ".._damage)
    end
end

function EventHandler.Unload()
    frame:SetScripts("OnEvent", "")
end


frame:SetScripts("OnEvent", "FalafelDoener.EventHandler:OnEvent(event, arg1, arg2,arg3,arg4,arg5,arg6,arg7,arg8)")
frame:RegisterEvent("VARIABLES_LOADED")
frame:RegisterEvent("LOADING_END")
frame:RegisterEvent("EXCHANGECLASS_SUCCESS")
--frame:RegisterEvent("COMBATMETER_DAMAGE")
frame:RegisterEvent("RIDE_INVITE_REQUEST")
frame:RegisterEvent("CURSOR_ITEM_UPDATE")
frame:RegisterEvent("SUIT_SKILL_PLATE_UPDATE")
frame:RegisterEvent("BAG_ITEM_UPDATE")

